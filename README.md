baosystems.tomcat
=================

Install Apache Tomcat from source or packages. Supports Tomcat 7 and higher on CentOS 7.

Requirements
------------

Developed against Ansible 2.9.

Role Variables
--------------

* `tomcat_install_method` - String. If set to "packages", install from dnf/yum (default for CentOS 7). If set to "source", install using upstream archive (default for CentOS 8).

* `tomcat_connector_compressiblemimetype` - String. Default is not set. If set, adds provided value as the argument to `compressibleMimeType` to all connectors in _/etc/tomcat/server.xml_.

* `tomcat_connector_compression` - String or integer. Default is not set. If set, adds provided value as the argument to `compression` as a string to all connectors in _/etc/tomcat/server.xml_.

* `tomcat_connector_compressionminsize` - Integer. Default is not set. If set, adds provided value as the argument to `compressionMinSize` to all connectors in _/etc/tomcat/server.xml_.

* `tomcat_connector_maxhttpheadersize` - Integer. Default is 65536, overriding the Tomcat default of 8192 (8 KiB). If set, adds provided value as the argument to `maxHttpHeaderSize` to all connectors in _/etc/tomcat/server.xml_.

* `tomcat_connector_maxthreads` - Integer. Default is not set. If set, adds provided value as the argument to `maxThreads` to all connectors in _/etc/tomcat/server.xml_.

* `tomcat_connector_nocompressionstrongetag` - Boolean. Default is not set. If set, adds provided value as the argument to `noCompressionStrongETag` to all connectors in _/etc/tomcat/server.xml_.

* `tomcat_connector_relaxedquerychars` - String. Default is not set. If set, adds provided value as the argument to `relaxedQueryChars` to all connectors in _/etc/tomcat/server.xml_.

* `tomcat_parameter_xms`: String. JAVA_OPTS option `-Xms`. Default is not set. Parameter is added in _/etc/tomcat/conf.d/memory.conf_.

* `tomcat_parameter_xmx`: String. JAVA_OPTS option `-Xmx`. Default is not set. Parameter is added in _/etc/tomcat/conf.d/memory.conf_.

* `tomcat_sample_webapps` - Boolean. Default is false. If true, install sample Tomcat webapps.

* `tomcat_version` - String. Ignored if `tomcat_install_method=packages`. Default is "8.5.99".

* `tomcat_conf_gc` - Boolean. Default is true. If true, create a Java 8 compatible _/etc/tomcat/conf.d/gc.conf_ file.

Dependencies
------------

Requires [geerlingguy.java](https://galaxy.ansible.com/geerlingguy/java) from Ansible Galaxy. See [meta/main.yml](https://bitbucket.org/baosystems/baosystems.tomcat/src/master/meta/main.yml#lines-2:5).

Change or set `java_packages` (list or string) to determine the RPM package name for Java JRE to install. See [geerlingguy/ansible-role-java/vars/](https://github.com/geerlingguy/ansible-role-java/tree/master/vars) for the default values set as `__java_packages`.

Example Playbook
----------------

    ---
    - hosts: all
      tasks:
        - include_role:
            name: baosystems.tomcat
          vars:
            java_packages: java-1.8.0-openjdk-headless
            tomcat_install_method: source
            tomcat_version: "8.5.99"
            tomcat_parameter_xms: 256m
            tomcat_parameter_xmx: 1g
            tomcat_sample_webapps: true
            tomcat_connector_compression: "on"
            tomcat_connector_compressiblemimetype: "text/html,text/xml,text/plain,text/css,text/javascript,application/javascript,application/json,application/xml,text/markdown"
            tomcat_connector_compressionminsize: "1024"
            tomcat_connector_nocompressionstrongetag: "false"
            tomcat_connector_maxhttpheadersize: "65536"
            tomcat_connector_maxthreads: "400"
            tomcat_connector_relaxedquerychars: "[]"
            tomcat_conf_gc: "false"

Extend Tomcat
-------------

In this role, _/etc/sysconfig/tomcat_ and _/etc/tomcat/tomcat.conf_ are managed by Ansible. It is recommended to **NOT** edit these files. Instead, create files in _/etc/tomcat/conf.d/_. These files support shell expansion, and can be grouped in logical files.

Below is an example of using the _/etc/tomcat/conf.d/*.conf_ pattern and using shell expansion so as to combine multiple `JAVA_OPTS` values:

```
# File: /etc/tomcat/conf.d/entropy.conf

JAVA_OPTS="$JAVA_OPTS -Djava.security.egd=file:/dev/./urandom"
```

```
# File: /etc/tomcat/conf.d/memory.conf

JAVA_OPTS="$JAVA_OPTS -Xms256m"

JAVA_OPTS="$JAVA_OPTS -Xmx1g"
```

When Tomcat starts, the fully resolved `JAVA_OPTS` will be shown with the `java` command like:

```
/usr/lib/jvm/jre/bin/java -Djava.security.egd=file:/dev/./urandom -Xms256m -Xmx1g ...
```

How it works
------------

When the installation method is set to _source_, this Ansible role will set up the destination system in a manner very similar to how the Tomcat 7 RPM packages behave on CentOS 7.

The most important concept to understand are the symbolic links in `/usr/share/tomcat/` and where they point to. Many of the paths are the same as the Tomcat RPMs, such as `/usr/share/tomcat/etc/` linking to `/etc/tomcat/`, with the exception of `/usr/share/tomcat/{bin,lib}` pointing to the expanded archive contents in `/opt/tomcat/current/{bin,lib}`. These paths are set in _/etc/tomcat/tomcat.conf_ and/or the files in _/opt/tomcat/libexec/_.

The following occurs with the `tomcat.service` systemd unit is started:

* Source the EnvironmentFile _/etc/tomcat/tomcat.conf_. This file contains important variables like `JAVA_HOME`, `CATALINA_HOME`, and `CATALINA_TMPDIR`. As multiple instances of Tomcat are supported, this file is sourced by all versions. This file is required by the systemd unit. Note that because Tomcat is ultimately started by `/opt/tomcat/libexec/server` and runs in the foreground, it is not necessary to set `CATALINA_PID`.

* The Environment variable `NAME` is set to be empty. As multiple instances of Tomcat are supported, having this blank ensures it is the "default" or primary instance of Tomcat.

* Source the EnvironmentFile _/etc/sysconfig/tomcat_. By default, there are no uncommented lines in this file and is present to mimic the Tomcat RPMs. This file is only referenced in the _tomcat.service_ unit for the "default" instance, NOT the multi-instance unit. This file is optional by the systemd unit. **It is NOT recommended to edit _/etc/sysconfig/tomcat_ or _/etc/tomcat/tomcat.conf_ and instead create files in _/etc/tomcat/conf.d/_!**

* Systemd runs the following as a foreground process: `/opt/tomcat/libexec/server start`. This is the same as the Tomcat RPMs but at a different path. The three files in `/opt/tomcat/libexec/` are copied from the Tomcat RPMs and set up the environment variables and Java paths to start Tomcat in the same manner as would happen from the _packages_ installation method.

License
-------

[MIT](https://bitbucket.org/baosystems/baosystems.tomcat/src/develop/LICENSE)

Author Information
------------------

* [Alan Ivey](https://github.com/alanivey/), BAO Systems
