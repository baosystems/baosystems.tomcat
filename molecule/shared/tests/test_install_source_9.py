import pytest


# Tomcat version
v = '9.0.86'


def test_archive_base(host):
    p = host.file(f'/opt/tomcat/apache-tomcat-{v}')

    assert p.is_directory
    assert p.user == 'root'
    assert p.group == 'tomcat'
    assert p.mode == 0o755


def test_archive_extracted(host):
    p = host.file(f'/opt/tomcat/apache-tomcat-{v}/RELEASE-NOTES')

    assert p.is_file
    assert p.user == 'root'
    assert p.group == 'root'
    assert p.mode == 0o644
    assert p.sha256sum == '7468045e8b0aa7706c3cf6650b7581842a12b050bbe68efdf9842f027fb08d48'


def test_catalinaout(host):
    p = host.file('/var/log/tomcat/catalina.out')

    assert p.is_file
    assert p.user == 'tomcat'
    assert p.group == 'tomcat'
    assert p.mode == 0o660

    content = p.content_string
    assert f'tomcat-{v} installed from source on 20' in content


def test_current_link(host):
    p = host.file('/opt/tomcat/current')

    assert p.is_symlink
    assert p.user == 'root'
    assert p.group == 'root'
    assert p.linked_to == f'/opt/tomcat/apache-tomcat-{v}'


# Version-specific files copied from this role
@pytest.mark.parametrize('path, owner, group, mode, sha256', [
    ('/etc/tomcat/catalina.policy', 'root', 'tomcat', 0o644,
        'c20ca30d920a34da4b428013b3ab4367b7d7d1f7d281e0560f48fd9612c2873d',),
    ('/etc/tomcat/catalina.properties', 'root', 'tomcat', 0o644,
        'c4f214b0030ce0a7e7b53bc309ac923bf8c8673fcc5ea7e32c2ec2ecc39fa168',),
    ('/etc/tomcat/context.xml', 'root', 'tomcat', 0o644,
        'e65d174393b7c9edb6966fa99478d5c3546eb124c6fbde748f8b13aa2ba04f02',),
    ('/etc/tomcat/jaspic-providers.xml', 'root', 'tomcat', 0o640,
        'f46d27e34323f6e40b72dfcc3cbee4952911d3d6f2ed366cc05e26922aea2c31',),
    ('/etc/tomcat/jaspic-providers.xsd', 'root', 'tomcat', 0o640,
        '1ba0dadb116c0dbcaafed8b5dcbfa8502a6eba800cfb1d62cadfe4e99c283ee5',),
    ('/etc/tomcat/logging.properties', 'root', 'tomcat', 0o644,
        '8ebb95116f2b00e185d911335fb09074b4f313c30e73b3a94c34ba0255f5feb8',),
    ('/etc/tomcat/tomcat-users.xml', 'root', 'tomcat', 0o640,
        '3ad69c98c920e69379b25a7878955fe265daa5c08de92afa7ce6ccb693b1358f',),
    ('/etc/tomcat/tomcat-users.xsd', 'root', 'tomcat', 0o640,
        '2c4fcb7ad719397904bea689b7154fb9c875afcadca788afffe38997bc386c75',),
    ('/etc/tomcat/web.xml', 'root', 'tomcat', 0o644,
        '1289204b41318c1a1e6798debefe4a574cd7ee62a6b7e22a0e13d16cea510f5e',),
])
def test_files(host, path, owner, group, mode, sha256):
    p = host.file(path)

    assert p.is_file
    assert p.user == owner
    assert p.group == group
    assert p.mode == mode
    assert p.sha256sum == sha256


# Same as above, and test for content
@pytest.mark.parametrize('path, owner, group, mode, sha256', [
    ('/etc/sysconfig/tomcat', 'root', 'root', 0o644,
        'cd8d509309273957d4e3db22aded56932f7e0a7396a12e4ded0f8b978ec745a6',),
    ('/etc/tomcat/tomcat.conf', 'root', 'tomcat', 0o644,
        'd8dc993351ce033b5a697501cbe9b45ea95c186721123a260253f545ff238478',),
])
def test_files_content(host, path, owner, group, mode, sha256):
    p = host.file(path)

    assert p.is_file
    assert p.user == owner
    assert p.group == group
    assert p.mode == mode
    assert p.sha256sum == sha256

    content = p.content_string
    assert 'DO NOT EDIT' in content


@pytest.mark.parametrize('path, target', [
    ('/usr/share/tomcat/bin', f'/opt/tomcat/apache-tomcat-{v}/bin',),
    ('/usr/share/tomcat/lib', f'/opt/tomcat/apache-tomcat-{v}/lib',),
])
def test_homedirlinks(host, path, target):
    p = host.file(path)

    assert p.is_symlink
    assert p.user == 'root'
    assert p.group == 'root'
    assert p.linked_to == target


@pytest.mark.parametrize('path, owner, group, mode, sha256', [
    ('/opt/tomcat/libexec/functions', 'root', 'root', 0o644,
        '07ff3ac90602ae5528ce4eccff929f1c46c87d99d5f92b3d9fae6d239f058afb'),
    ('/opt/tomcat/libexec/preamble', 'root', 'root', 0o755,
        '88d4a5c7e1c7bb324c18630a3c848bafdf2ef44413ef1cc5ff35f8483a83b316'),
    ('/opt/tomcat/libexec/server', 'root', 'root', 0o755,
        '7f7df3c460e51783cf9231104d7b5e6c14224c47f7974286a21e06d70b7f9a48'),
])
def test_libexec_files(host, path, owner, group, mode, sha256):
    p = host.file(path)

    assert p.is_file
    assert p.user == owner
    assert p.group == group
    assert p.mode == mode
    assert p.sha256sum == sha256


# Paths to be deleted from downloaded archive.
@pytest.mark.parametrize('path', [
    f'/opt/tomcat/apache-tomcat-{v}/conf',
    f'/opt/tomcat/apache-tomcat-{v}/logs',
    f'/opt/tomcat/apache-tomcat-{v}/temp',
    f'/opt/tomcat/apache-tomcat-{v}/webapps',
    f'/opt/tomcat/apache-tomcat-{v}/work',
    f'/opt/tomcat/apache-tomcat-{v}/BUILDING.txt',
    f'/opt/tomcat/apache-tomcat-{v}/CONTRIBUTING.md',
    f'/opt/tomcat/apache-tomcat-{v}/LICENSE',
    f'/opt/tomcat/apache-tomcat-{v}/NOTICE',
    f'/opt/tomcat/apache-tomcat-{v}/README.md',
])
def test_source_removedunused(host, path):
    p = host.file(path)
    assert not p.exists
