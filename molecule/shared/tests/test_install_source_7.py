import pytest


# Tomcat version
v = '7.0.109'


def test_archive_base(host):
    p = host.file(f'/opt/tomcat/apache-tomcat-{v}')

    assert p.is_directory
    assert p.user == 'root'
    assert p.group == 'tomcat'
    assert p.mode == 0o755


def test_archive_extracted(host):
    p = host.file(f'/opt/tomcat/apache-tomcat-{v}/RELEASE-NOTES')

    assert p.is_file
    assert p.user == 'root'
    assert p.group == 'root'
    assert p.mode == 0o644
    assert p.sha256sum == '0eb1fd7e181b873f7fcda26dffaac2a80ed3e22e7d9b8d23e705c92eb54f697e'


def test_catalinaout(host):
    p = host.file('/var/log/tomcat/catalina.out')

    assert p.is_file
    assert p.user == 'tomcat'
    assert p.group == 'tomcat'
    assert p.mode == 0o660

    content = p.content_string
    assert f'tomcat-{v} installed from source on 20' in content


def test_current_link(host):
    p = host.file('/opt/tomcat/current')

    assert p.is_symlink
    assert p.user == 'root'
    assert p.group == 'root'
    assert p.linked_to == f'/opt/tomcat/apache-tomcat-{v}'


# Version-specific files copied from this role
@pytest.mark.parametrize('path, owner, group, mode, sha256', [
    ('/etc/tomcat/catalina.policy', 'root', 'tomcat', 0o644,
        '051b7829645216d7369aea19e1c358e4f428d7308222ccd6603c82ab6088145d',),
    ('/etc/tomcat/catalina.properties', 'root', 'tomcat', 0o644,
        'a200408358a38894d0a48b22d009914375d4c6e2d5658ee46b8a77ae4bd68732',),
    ('/etc/tomcat/context.xml', 'root', 'tomcat', 0o644,
        'c4ec08ad603fd5a524c6088c6510476b20d64b819fba7ce1a4221d305b2220ca',),
    ('/etc/tomcat/jaspic-providers.xml', 'root', 'tomcat', 0o640,
        'f46d27e34323f6e40b72dfcc3cbee4952911d3d6f2ed366cc05e26922aea2c31',),
    ('/etc/tomcat/jaspic-providers.xsd', 'root', 'tomcat', 0o640,
        '1ba0dadb116c0dbcaafed8b5dcbfa8502a6eba800cfb1d62cadfe4e99c283ee5',),
    ('/etc/tomcat/logging.properties', 'root', 'tomcat', 0o644,
        'aee6ed6c361c5f083aac87fb40cf867e329380ee878f98d8085ffbe40e5bd093',),
    ('/etc/tomcat/tomcat-users.xml', 'root', 'tomcat', 0o640,
        '62f30d402e95d1417fa73b5c74e2f7849b5c50c909d766c5a1c456410f7ff7a1',),
    ('/etc/tomcat/tomcat-users.xsd', 'root', 'tomcat', 0o640,
        '763c92273d9b1643c9e9726673dbba09f7d7b6dfda2d0cb024d5acc526c94f00',),
    ('/etc/tomcat/web.xml', 'root', 'tomcat', 0o644,
        'c19c23dcc7fb49a4261028792b5147d0db57341bda7d19f3cc9a95d1e9cee885',),
])
def test_files(host, path, owner, group, mode, sha256):
    p = host.file(path)

    assert p.is_file
    assert p.user == owner
    assert p.group == group
    assert p.mode == mode
    assert p.sha256sum == sha256


# Same as above, and test for content
@pytest.mark.parametrize('path, owner, group, mode, sha256', [
    ('/etc/sysconfig/tomcat', 'root', 'root', 0o644,
        '0884c576d53410d3b312723c53903df42921e8276ffa206de3641872852f224b',),
    ('/etc/tomcat/tomcat.conf', 'root', 'tomcat', 0o644,
        'd8dc993351ce033b5a697501cbe9b45ea95c186721123a260253f545ff238478',),
])
def test_files_content(host, path, owner, group, mode, sha256):
    p = host.file(path)

    assert p.is_file
    assert p.user == owner
    assert p.group == group
    assert p.mode == mode
    assert p.sha256sum == sha256

    content = p.content_string
    assert 'DO NOT EDIT' in content


@pytest.mark.parametrize('path, target', [
    ('/usr/share/tomcat/bin', f'/opt/tomcat/apache-tomcat-{v}/bin',),
    ('/usr/share/tomcat/lib', f'/opt/tomcat/apache-tomcat-{v}/lib',),
])
def test_homedirlinks(host, path, target):
    p = host.file(path)

    assert p.is_symlink
    assert p.user == 'root'
    assert p.group == 'root'
    assert p.linked_to == target


@pytest.mark.parametrize('path, owner, group, mode, sha256', [
    ('/opt/tomcat/libexec/functions', 'root', 'root', 0o644,
        '1d4b072ea99d472246fd60dcabf7dcad09049e9f3282d28cb44ee0fa98240660'),
    ('/opt/tomcat/libexec/preamble', 'root', 'root', 0o755,
        '88d4a5c7e1c7bb324c18630a3c848bafdf2ef44413ef1cc5ff35f8483a83b316'),
    ('/opt/tomcat/libexec/server', 'root', 'root', 0o755,
        '7f7df3c460e51783cf9231104d7b5e6c14224c47f7974286a21e06d70b7f9a48'),
])
def test_libexec_files(host, path, owner, group, mode, sha256):
    p = host.file(path)

    assert p.is_file
    assert p.user == owner
    assert p.group == group
    assert p.mode == mode
    assert p.sha256sum == sha256


# Paths to be deleted from downloaded archive.
@pytest.mark.parametrize('path', [
    f'/opt/tomcat/apache-tomcat-{v}/conf',
    f'/opt/tomcat/apache-tomcat-{v}/logs',
    f'/opt/tomcat/apache-tomcat-{v}/temp',
    f'/opt/tomcat/apache-tomcat-{v}/webapps',
    f'/opt/tomcat/apache-tomcat-{v}/work',
    f'/opt/tomcat/apache-tomcat-{v}/BUILDING.txt',
    f'/opt/tomcat/apache-tomcat-{v}/CONTRIBUTING.md',
    f'/opt/tomcat/apache-tomcat-{v}/LICENSE',
    f'/opt/tomcat/apache-tomcat-{v}/NOTICE',
    f'/opt/tomcat/apache-tomcat-{v}/README.md',
])
def test_source_removedunused(host, path):
    p = host.file(path)
    assert not p.exists
