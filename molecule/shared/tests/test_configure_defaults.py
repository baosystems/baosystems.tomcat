def test_template_serverxml_defaults(host):
    f = host.file('/etc/tomcat/server.xml')

    content = f.content_string

    assert 'compression="' not in content
    assert 'compressibleMimeTypehtml="' not in content
    assert 'compressionMinSize="' not in content
    assert 'noCompressionStrongETag="' not in content
    assert 'maxThreads="' not in content
    assert 'relaxedQueryChars="' not in content
