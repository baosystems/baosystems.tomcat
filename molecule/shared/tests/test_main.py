# From meta/main.yml: geerlingguy.java
def test_pkgs(host):
    p = host.package('java-1.8.0-openjdk-headless')
    assert p.is_installed
