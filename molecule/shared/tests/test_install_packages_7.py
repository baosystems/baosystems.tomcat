import pytest


@pytest.mark.parametrize('pkg', [
    'tomcat',
    'tomcat-lib',
])
def test_pkgs(host, pkg):
    p = host.package(pkg)
    assert p.is_installed


# Version-specific files copied from this role
@pytest.mark.parametrize('path, owner, group, mode, sha256', [
    ('/etc/sysconfig/tomcat', 'root', 'root', 0o644,
        '0884c576d53410d3b312723c53903df42921e8276ffa206de3641872852f224b',),
    ('/etc/tomcat/tomcat.conf', 'root', 'tomcat', 0o644,
        '68d8bda9164eb4baa14fa4074adf27314ef2787732e71e1cf01f7f525ceae99e',),
])
def test_files(host, path, owner, group, mode, sha256):
    p = host.file(path)

    assert p.is_file
    assert p.user == owner
    assert p.group == group
    assert p.mode == mode
    assert p.sha256sum == sha256

    content = p.content_string
    assert 'DO NOT EDIT' in content


# Files from the Tomcat 7 RPM
@pytest.mark.parametrize('path, owner, group, mode, sha256', [
    ('/etc/tomcat/catalina.policy', 'root', 'tomcat', 0o644,
        '6317988ac327b91af01843cdf16c3b5b2774b659dc449411323712177ff5f708',),
    ('/etc/tomcat/catalina.properties', 'root', 'tomcat', 0o644,
        'e7700c4945f14ae17ce6be1eae121b16602d31df280262736a9fd4d2c6e34143',),
    ('/etc/tomcat/context.xml', 'root', 'tomcat', 0o644,
        '2e602c69073700153de1b60f10c17b424ef151336315df1be1ee4581c2480f26',),
    ('/etc/tomcat/log4j.properties', 'root', 'tomcat', 0o644,
        '0da536b6175b37c945618b56197283b60a6ad2cdf95c21f6102c655752da7d86',),
    ('/etc/tomcat/logging.properties', 'root', 'tomcat', 0o644,
        '8a2e31b09ed07007a18155ea418568589ae415f82e753eccaa35682404720b26',),
    ('/etc/tomcat/tomcat-users.xml', 'root', 'tomcat', 0o640,
        '6880593f15ec565a465a26169f3a68c290a06b7dd753cd151d618940ebc6b8ba',),
    ('/etc/tomcat/web.xml', 'root', 'tomcat', 0o644,
        '34da27a274c25dc07d5295c25509eb9d7c52337b3dc3c5d7cb1e481361eda1c3',),
])
def test_files_fromrpm(host, path, owner, group, mode, sha256):
    p = host.file(path)

    assert p.is_file
    assert p.user == owner
    assert p.group == group
    assert p.mode == mode
    assert p.sha256sum == sha256
