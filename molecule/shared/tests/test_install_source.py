import pytest


def test_parent_directory(host):
    p = host.file('/opt/tomcat')

    assert p.is_directory
    assert p.user == 'root'
    assert p.group == 'root'
    assert p.mode == 0o755


@pytest.mark.parametrize('path, owner, group, mode', [
    ('/etc/tomcat', 'root', 'tomcat', 0o755),
    ('/var/cache/tomcat', 'root', 'tomcat', 0o770),
    ('/var/cache/tomcat/temp', 'root', 'tomcat', 0o770),
    ('/var/cache/tomcat/work', 'root', 'tomcat', 0o770),
    ('/var/lib/tomcat', 'root', 'tomcat', 0o755),
    ('/var/lib/tomcat/webapps', 'root', 'tomcat', 0o775),
    ('/var/log/tomcat', 'tomcat', 'root', 0o770),
    ('/var/run/tomcat', 'tomcat', 'root', 0o750),
])
def test_homedirs(host, path, owner, group, mode):
    p = host.file(path)

    assert p.is_directory
    assert p.user == owner
    assert p.group == group
    assert p.mode == mode


@pytest.mark.parametrize('path, target', [
    ('/usr/share/tomcat/conf', '/etc/tomcat',),
    ('/usr/share/tomcat/logs', '/var/log/tomcat',),
    ('/usr/share/tomcat/temp', '/var/cache/tomcat/temp',),
    ('/usr/share/tomcat/webapps', '/var/lib/tomcat/webapps',),
    ('/usr/share/tomcat/work', '/var/cache/tomcat/work',),
])
def test_homedirlinks(host, path, target):
    p = host.file(path)

    assert p.is_symlink
    assert p.user == 'root'
    assert p.group == 'root'
    assert p.linked_to == target


@pytest.mark.parametrize('path, owner, group, mode', [
    ('/etc/tomcat/Catalina', 'root', 'tomcat', 0o775),
    ('/etc/tomcat/Catalina/localhost', 'root', 'tomcat', 0o775),
    ('/etc/tomcat/conf.d', 'root', 'tomcat', 0o755),
])
def test_etcdirs(host, path, owner, group, mode):
    p = host.file(path)

    assert p.is_directory
    assert p.user == owner
    assert p.group == group
    assert p.mode == mode


def test_etcconfdreadme(host):
    p = host.file('/etc/tomcat/conf.d/README')

    assert p.is_file
    assert p.user == 'root'
    assert p.group == 'tomcat'
    assert p.mode == 0o644
    assert p.sha256sum == 'd4762dbc9c12d09ab3391f1aac6b6d93a17605c25f972e3b3f3b6d8eec83a697'


def test_logrotate(host):
    p = host.file('/etc/logrotate.d/tomcat')

    assert p.is_file
    assert p.user == 'root'
    assert p.group == 'root'
    assert p.mode == 0o644
    assert p.sha256sum == 'cd1a932f816f231f48d41fbe9c3abd9772890a03e285b183d5ef8024fc38fdd5'


def test_libexec_directory(host):
    p = host.file('/opt/tomcat/libexec')

    assert p.is_directory
    assert p.user == 'root'
    assert p.group == 'root'
    assert p.mode == 0o755


# Test for sample webapps to NOT be present.
@pytest.mark.parametrize('path', [
    '/var/lib/tomcat/webapps/docs',
    '/var/lib/tomcat/webapps/examples',
    '/var/lib/tomcat/webapps/host-manager',
    '/var/lib/tomcat/webapps/manager',
    '/var/lib/tomcat/webapps/ROOT/tomcat-power.gif',
])
def test_samplewebapps_removed(host, path):
    p = host.file(path)
    assert not p.exists
