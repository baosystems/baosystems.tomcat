import pytest


# Files copied from this role
@pytest.mark.parametrize('path, owner, group, mode, sha256', [
    ('/etc/cron.daily/tomcat_logs', 'root', 'root', 0o755,
        '59df32168e39896ee3aa69c280f70db001c499569fbfc520f2272e546f2d35f1',),
    ('/etc/tomcat/conf.d/entropy.conf', 'root', 'tomcat', 0o644,
        '78f4652d6bce1d2a5bc04f41e42a1e4511dac15d8938c65eaa10393a59aeb58e',),
    ('/etc/tomcat/conf.d/gc.conf', 'root', 'tomcat', 0o644,
        'a0331605eca904a6dff2df6ba3be321f9c1aeddc72668e1fcc5a084f0266f8ef',),
])
def test_files(host, path, owner, group, mode, sha256):
    p = host.file(path)

    assert p.is_file
    assert p.user == owner
    assert p.group == group
    assert p.mode == mode
    assert p.sha256sum == sha256


def test_file_oldcron(host):
    f = host.file('/etc/cron.daily/tomcat_logs.sh')
    assert not f.exists


# Template copied from this role
def test_template_serverxml(host):
    f = host.file('/etc/tomcat/server.xml')

    assert f.is_file
    assert f.user == 'root'
    assert f.group == 'tomcat'
    assert f.mode == 0o644

    content = f.content_string
    assert 'scheme="https"' in content
    assert 'secure="true"' in content
