import pytest


# Test for sample webapps to NOT be present.
@pytest.mark.parametrize('pkg', [
    'tomcat-admin-webapps',
    'tomcat-docs-webapp',
    'tomcat-webapps',
])
def test_samplewebapps_removed(host, pkg):
    p = host.package(pkg)
    assert not p.is_installed
