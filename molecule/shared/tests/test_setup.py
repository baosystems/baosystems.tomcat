import pytest


@pytest.mark.parametrize('pkg', [
    'crontabs',
    'javapackages-tools',
    'logrotate',
    'tar',
    'unzip',
])
def test_pkgs(host, pkg):
    p = host.package(pkg)
    assert p.is_installed
