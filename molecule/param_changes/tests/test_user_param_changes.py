def test_tomcat_home(host):
    d = host.file('/usr/share/tomcat')

    assert d.is_directory
    assert d.uid == 0
    assert d.gid == 91
    assert d.mode == 0o775


def test_tomcat_group(host):
    g = host.group('tomcat')

    assert g.gid == 91


def test_tomcat_user(host):
    u = host.user('tomcat')

    assert u.gecos == 'Apache Tomcat'
    assert u.uid == 91
    assert u.group == 'tomcat'
    assert u.home == '/usr/share/tomcat'
    assert u.shell == '/sbin/nologin'
