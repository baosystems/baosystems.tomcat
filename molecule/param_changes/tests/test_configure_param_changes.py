import pytest


def test_template_serverxml_param_changes(host):
    f = host.file('/etc/tomcat/server.xml')

    content = f.content_string

    assert 'compression="force"' in content
    assert 'compressibleMimeType="text/html,text/xml,text/plain,text/css,text/javascript,application/javascript,application/json,application/xml,text/markdown"' in content  # noqa 501
    assert 'compressionMinSize="20"' in content
    assert 'noCompressionStrongETag="false"' in content
    assert 'maxThreads="300"' in content
    assert 'relaxedQueryChars="[]"' in content


# Template file created when tomcat_parameter_xms or tomcat_parameter_xmx
# are present.
def test_template_memory(host):
    f = host.file('/etc/tomcat/conf.d/memory.conf')

    assert f.is_file
    assert f.user == 'root'
    assert f.group == 'tomcat'
    assert f.mode == 0o644

    content = f.content_string
    assert 'JAVA_OPTS="$JAVA_OPTS -Xms256m"' in content
    assert 'JAVA_OPTS="$JAVA_OPTS -Xmx1g"' in content


# Test for sample webapps to be present.
@pytest.mark.parametrize('pkg', [
    'tomcat-admin-webapps',
    'tomcat-docs-webapp',
    'tomcat-webapps',
])
def test_samplewebapps_installed(host, pkg):
    p = host.package(pkg)
    assert p.is_installed
