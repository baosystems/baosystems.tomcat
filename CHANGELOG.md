# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.6.2](https://bitbucket.org/baosystems/baosystems.tomcat/compare/1.6.2%0D1.6.1#diff) (2024-02-26)


### Bug Fixes

* bump Tomcat 8 ([497b284](https://bitbucket.org/baosystems/baosystems.tomcat/commits/497b284411b8c22b5c3a50ebac0877ccf9909565))
* bump Tomcat 9 ([9f897ef](https://bitbucket.org/baosystems/baosystems.tomcat/commits/9f897effb0ec1a371dc1cf32ad2e45dcf788b604))


### CI

* specify Python 3.8 ([8e74e18](https://bitbucket.org/baosystems/baosystems.tomcat/commits/8e74e18fd2e27891df07d8d33caadcb799dcdfbe))
* update ca-certificates right away ([ff74463](https://bitbucket.org/baosystems/baosystems.tomcat/commits/ff7446388a83cf065d94edfa034afce1011c268e))


### Docs

* local command for running on EC2 instance ([272e7d6](https://bitbucket.org/baosystems/baosystems.tomcat/commits/272e7d632c4c1d96419a836a74dd0a081a047087))

## [1.6.1](https://bitbucket.org/baosystems/baosystems.tomcat/compare/1.6.1%0D1.6.0#diff) (2024-01-10)


### Bug Fixes

* 8.5.95 -> 8.5.98 ([da48b37](https://bitbucket.org/baosystems/baosystems.tomcat/commits/da48b3738e97f5192367a5a214fa52dbc7373947))
* 9.0.82 -> 9.0.85 ([86ec926](https://bitbucket.org/baosystems/baosystems.tomcat/commits/86ec926d40987fbe3e938d91e6adae7fc7658352))

## [1.6.0](https://bitbucket.org/baosystems/baosystems.tomcat/compare/1.6.0%0D1.5.3#diff) (2023-11-03)


### Features

* add tomcat_connector_maxhttpheadersize=65536 ([a28e151](https://bitbucket.org/baosystems/baosystems.tomcat/commits/a28e15128ceca7b31c7edd515729bced9d70d488))


### Bug Fixes

* bump to the latest version of Tomcat 8 ([9f273b0](https://bitbucket.org/baosystems/baosystems.tomcat/commits/9f273b01c6eda91d6416df6df11858cee5ad589b))
* bump to the latest version of Tomcat 9 ([a4d0d33](https://bitbucket.org/baosystems/baosystems.tomcat/commits/a4d0d33e03c1e805af18c80284df7d0445b9ec73))


### Docs

* grammar ([50787f6](https://bitbucket.org/baosystems/baosystems.tomcat/commits/50787f62084852c2f386ebc307f70ac66514dd66))
* typofix ([f3e6c51](https://bitbucket.org/baosystems/baosystems.tomcat/commits/f3e6c5174bfdfd6b223d50d77c53d750cc399ba9))

## [1.5.3](https://bitbucket.org/baosystems/baosystems.tomcat/compare/1.5.3%0D1.5.2#diff) (2023-09-07)


### Bug Fixes

* bump to latest versions of Tomcat 8 and 9 ([7ac5265](https://bitbucket.org/baosystems/baosystems.tomcat/commits/7ac52657fbd18b9e675546e1a99ff9fea298fe5b))

## [1.5.2](https://bitbucket.org/baosystems/baosystems.tomcat/compare/1.5.2%0D1.5.1#diff) (2023-07-14)


### Bug Fixes

* bump Tomcat 8.5 and 9 ([a93d106](https://bitbucket.org/baosystems/baosystems.tomcat/commits/a93d1069bbaf01737c35cb60b1d7b2e3b3bc4330))

## [1.5.1](https://bitbucket.org/baosystems/baosystems.tomcat/compare/1.5.1%0D1.5.0#diff) (2023-07-14)


### Bug Fixes

* bump Ansible, specify others ([db87488](https://bitbucket.org/baosystems/baosystems.tomcat/commits/db87488535bcebd3663b1b416b87d78729364bfb))
* lower version of rich ([e4f4b25](https://bitbucket.org/baosystems/baosystems.tomcat/commits/e4f4b2551e3a285a2b87304a0bf985910bec44e8))
* tomcat user can access application directory root ([d80e2a5](https://bitbucket.org/baosystems/baosystems.tomcat/commits/d80e2a59aa471b1b37e545eb8a0744ce99ecfb9e))

## [1.5.0](https://bitbucket.org/baosystems/baosystems.tomcat/compare/1.5.0%0D1.4.2#diff) (2022-11-15)


### Features

* bump to latest versions ([acd577a](https://bitbucket.org/baosystems/baosystems.tomcat/commits/acd577ad0ecf0a72bb1039a30edaf25b409504f5))


### Bug Fixes

* update checksum ([5abd61f](https://bitbucket.org/baosystems/baosystems.tomcat/commits/5abd61f3f7756d9ab76d1cbad78a301b97a96de3))

## [1.4.2](https://bitbucket.org/baosystems/baosystems.tomcat/compare/1.4.2%0D1.4.1#diff) (2022-03-23)


### Bug Fixes

* update Tomcat 8.5 to 8.5.77 ([466465d](https://bitbucket.org/baosystems/baosystems.tomcat/commits/466465d383ff957040e1093b4750e7aa09587be7))
* update Tomcat 9 to 9.0.60 ([48d60de](https://bitbucket.org/baosystems/baosystems.tomcat/commits/48d60def5493024606df1612b5e4600f2edb7acb))


### Tests

* update checksum for Tomcat 9 ([f9ff1f6](https://bitbucket.org/baosystems/baosystems.tomcat/commits/f9ff1f6f8bca23e72fa96a257f04e96e95f6cd45))

## [1.4.1](https://bitbucket.org/baosystems/baosystems.tomcat/compare/1.4.1%0D1.4.0#diff) (2021-10-21)


### Bug Fixes

* parse as boolean not string ([0032451](https://bitbucket.org/baosystems/baosystems.tomcat/commits/0032451a764428c126a09158d38cbf2db069e765))

## [1.4.0](https://bitbucket.org/baosystems/baosystems.tomcat/compare/1.4.0%0D1.3.3#diff) (2021-10-20)


### Features

* boolean "tomcat_conf_gc" for /etc/tomcat/conf.d/gc.conf ([acb40bd](https://bitbucket.org/baosystems/baosystems.tomcat/commits/acb40bd546d600f2761433a2a1b34cb15aacf675))


### Bug Fixes

* don't enable either ([b816641](https://bitbucket.org/baosystems/baosystems.tomcat/commits/b816641ce2aa079db6b465123aace83acd624ad0))
* don't start at the end, leave that to the operator ([4bfa35c](https://bitbucket.org/baosystems/baosystems.tomcat/commits/4bfa35c7f8be3f6644cc13845366a509080c5d32))
* enable Tomcat service on boot ([cc355cb](https://bitbucket.org/baosystems/baosystems.tomcat/commits/cc355cb0fdb811e4bff77a79a1cd7a32ba656824))
* enabled should not have been here ([52c02fc](https://bitbucket.org/baosystems/baosystems.tomcat/commits/52c02fca7804b86f8700fe3b4f29dac81766a497))
* move service enable task to more appropriate file ([6be7250](https://bitbucket.org/baosystems/baosystems.tomcat/commits/6be725063039b36cea6be7f93b53f722904caa12))


### Others

* clarify comment ([94615d2](https://bitbucket.org/baosystems/baosystems.tomcat/commits/94615d2d72c6d63acac07bbac47d9f7f201f768e))
* typofix ([bd7427f](https://bitbucket.org/baosystems/baosystems.tomcat/commits/bd7427fb221ba43353c947f4d60ac1a8e8dae56b))

## [1.3.3](https://bitbucket.org/baosystems/baosystems.tomcat/compare/1.3.3%0D1.3.2#diff) (2021-09-24)


### Bug Fixes

* update Tomcat 7 to 7.0.109 ([8f20192](https://bitbucket.org/baosystems/baosystems.tomcat/commits/8f20192be31192774702fc0fa1efec20d30b7bd8))
* update Tomcat 7 to track upstream package configuration ([e819ccf](https://bitbucket.org/baosystems/baosystems.tomcat/commits/e819ccff9d3ed98429cee236043625ade8bf59d2))
* update Tomcat 8 to 8.5.66 ([687fba6](https://bitbucket.org/baosystems/baosystems.tomcat/commits/687fba6114c1261742ec228c14e17b80d792cf93))
* update Tomcat 8 to 8.5.71 ([9d3ebb8](https://bitbucket.org/baosystems/baosystems.tomcat/commits/9d3ebb8314b6df99ee3c4d72f6e3d79f8453cc6d))
* update Tomcat 9 to 9.0.46 ([1884564](https://bitbucket.org/baosystems/baosystems.tomcat/commits/1884564ead27d25425b4fcad68d73bfa2eb96501))
* update Tomcat 9 to 9.0.53 ([56ea90c](https://bitbucket.org/baosystems/baosystems.tomcat/commits/56ea90c12a1fb6eea6884d662745427f71b0a736))


### Docs

* point to directory of test files ([5955a3c](https://bitbucket.org/baosystems/baosystems.tomcat/commits/5955a3cbfc57f22f7d9b5b1c8f809607be146ee4))


### Others

* consistent formatting ([f8dfe08](https://bitbucket.org/baosystems/baosystems.tomcat/commits/f8dfe0884571e8b39924044e259a02fc9827f4eb))
* **gitignore:** ignore .cache directories ([7845081](https://bitbucket.org/baosystems/baosystems.tomcat/commits/784508157ad734a37e572c7849ef56317e87278e))
* **molecule:** use default image in podman scenario ([d26f562](https://bitbucket.org/baosystems/baosystems.tomcat/commits/d26f5626e6b70deb0520f250d8b2c9852a11973a))
* **pipenv:** restore functionality ([c10ec93](https://bitbucket.org/baosystems/baosystems.tomcat/commits/c10ec93dbcfd3439605b5329ec7828bb9545f089))
* tomcat 9 to track Fedora 34 ([caba8a1](https://bitbucket.org/baosystems/baosystems.tomcat/commits/caba8a1db62f58bffe02de4d2285bb315f72a1dd))
* typofix ([b012133](https://bitbucket.org/baosystems/baosystems.tomcat/commits/b012133ffd41a08721bb0ac28b5047f00e730931))


### Tests

* bump version for docker tests ([83cffdb](https://bitbucket.org/baosystems/baosystems.tomcat/commits/83cffdba7815b3a70f266d4ad8698a956b0b2a25))
* bump version for podman tests ([c31f563](https://bitbucket.org/baosystems/baosystems.tomcat/commits/c31f5630d4434180d9a6fad9e4b51005f0a20b59))
* forgot to include this earlier ([79fdb54](https://bitbucket.org/baosystems/baosystems.tomcat/commits/79fdb54a6ec91154a2daced9bb769258dddc3bff))
* rename source to source_8 ([d25fcad](https://bitbucket.org/baosystems/baosystems.tomcat/commits/d25fcad95b783dfed7ed0408699f37ff3b17eb7e))
* sha256sum for updated Tomcat 7 files ([b9ea8f5](https://bitbucket.org/baosystems/baosystems.tomcat/commits/b9ea8f549c8842f5115a942f1b6fd0f6c18489ad))
* sha256sum for updated Tomcat 9 files ([801ac13](https://bitbucket.org/baosystems/baosystems.tomcat/commits/801ac13f4c5ec8eb71ab1c2b361dd881a1de3fb9))
* sha256sums for updated Tomcat 7 files ([e505454](https://bitbucket.org/baosystems/baosystems.tomcat/commits/e505454f2ecee2e34375f62bafeeea693718de39))
* use version variable more ([b4c6cce](https://bitbucket.org/baosystems/baosystems.tomcat/commits/b4c6cceef823f65228c1dd76ae9bbd88f8814862))

## [1.3.2](https://bitbucket.org/baosystems/baosystems.tomcat/compare/1.3.2%0D1.3.1#diff) (2021-01-14)


### Bug Fixes

* multiple, see below ([1b6fef4](https://bitbucket.org/baosystems/baosystems.tomcat/commits/1b6fef49a407a986dafb8fb84e80aea1f642e9f1))


### CI

* ensure single key attempted for ssh ([975f320](https://bitbucket.org/baosystems/baosystems.tomcat/commits/975f32070fe2749ca69ba771b78491d587187b59))
* run ec2_source_9 in pipelines ([782cceb](https://bitbucket.org/baosystems/baosystems.tomcat/commits/782ccebe06c16c7d5ef9cc4a717ca61373a92964))


### Others

* remove extra platforms ([0264150](https://bitbucket.org/baosystems/baosystems.tomcat/commits/0264150a3f8094459a11a101fa002e26116bbb41))
* remove unnecessary section ([f7cd032](https://bitbucket.org/baosystems/baosystems.tomcat/commits/f7cd032751af729ddeab78b6a8a69e7ca32197a3))
* tidy ([3a594d3](https://bitbucket.org/baosystems/baosystems.tomcat/commits/3a594d33b166c239249988dca2da29c68eced381))

## [1.3.1](https://bitbucket.org/baosystems/baosystems.tomcat/compare/1.3.1%0D1.3.0#diff) (2020-12-29)


### Bug Fixes

* ensure old tomcat_logs.sh is removed ([ee1bcab](https://bitbucket.org/baosystems/baosystems.tomcat/commits/ee1bcabfc258d60b5efe304a80f82aa0bd3dd70b))


### Others

* remove PIPENV_VENV_IN_PROJECT from .envrc ([462890f](https://bitbucket.org/baosystems/baosystems.tomcat/commits/462890f273607d5906c0a9fe84442914c8d3f962))
* reorder for clarity ([9743767](https://bitbucket.org/baosystems/baosystems.tomcat/commits/9743767b0d968adab5f84d46ca15f9cc64190cc0))


### CI

* bump atlassian/bitbucket-upload-file ([0ce635b](https://bitbucket.org/baosystems/baosystems.tomcat/commits/0ce635b00ed7a4e457f0d8a5c1fb28ee2cb74165))
* increase git clone depth ([7adaf62](https://bitbucket.org/baosystems/baosystems.tomcat/commits/7adaf625f645c1ad5171d03b3c3db49955454063))

## [1.3.0](https://bitbucket.org/baosystems/baosystems.tomcat/compare/1.3.0%0D1.2.0#diff) (2020-10-19)


### Features

* **upstream:** bump default Tomcat version ([483700c](https://bitbucket.org/baosystems/baosystems.tomcat/commits/483700c4822cc013ae67059c42264974d5cbde99))


### Bug Fixes

* resolve ansible-lint 208 ([585e2ef](https://bitbucket.org/baosystems/baosystems.tomcat/commits/585e2ef560adf22199d9a4428ac098fdc576b67b))


### CI

* bump atlassian/bitbucket-upload-file ([b56ef74](https://bitbucket.org/baosystems/baosystems.tomcat/commits/b56ef74b965474a8de36baec9f204048e71406c1))
* fix syntax error ([d8b6827](https://bitbucket.org/baosystems/baosystems.tomcat/commits/d8b682772e15dd44ac6e475494618fc4666e5edd))
* fix syntax error ([b5f1506](https://bitbucket.org/baosystems/baosystems.tomcat/commits/b5f1506a97cc511bfe7a8bf598ba7fee00866ab3))
* formatting, bump standard-version ([6739101](https://bitbucket.org/baosystems/baosystems.tomcat/commits/6739101192013fc3297554363817ab51f2a855bd))
* include an about file instead of copying the readme ([1b14824](https://bitbucket.org/baosystems/baosystems.tomcat/commits/1b14824ee9bec5ff807305bb94a3faf49966fd77))
* include LICENSE in archives ([ce4c4f7](https://bitbucket.org/baosystems/baosystems.tomcat/commits/ce4c4f75d4e93cb2d09442b2165c6180644404f7))
* refactor pipelines file using anchors ([b3fdf88](https://bitbucket.org/baosystems/baosystems.tomcat/commits/b3fdf8851bd9424a7995be624b32a50cbddfe09b))
* releases can happen in parallel with master branch archive ([83fabe8](https://bitbucket.org/baosystems/baosystems.tomcat/commits/83fabe835a1cb95f1624744c847a46af5906c36f))
* remove ec2/prepare.yml ([148283b](https://bitbucket.org/baosystems/baosystems.tomcat/commits/148283b09c25b2c101770e89884d5184c8068a07))
* run bitbucket-upload-file once ([06dea21](https://bitbucket.org/baosystems/baosystems.tomcat/commits/06dea2187c80782a2506b1204f34bc3f2ca70178))
* use base images ([25f4b76](https://bitbucket.org/baosystems/baosystems.tomcat/commits/25f4b76d4e6c75d2d30299c778ce4d9a7c1be6b4))


### Docs

* update for latest Tomcat source releases ([67264bc](https://bitbucket.org/baosystems/baosystems.tomcat/commits/67264bca76342eee09fe9b930b876a9ed4ccaac7))


### Others

* format same as other roles ([5d775ec](https://bitbucket.org/baosystems/baosystems.tomcat/commits/5d775ec913a51110fbadd9be23010db4eb57a4af))
* improve comments ([9114dc5](https://bitbucket.org/baosystems/baosystems.tomcat/commits/9114dc5daab2f08a764e99eb3b35d35baf92255d))


### Build System

* **pre-commit:** bump hooks ([c18491b](https://bitbucket.org/baosystems/baosystems.tomcat/commits/c18491bbb38028b373349fa5b641d0ac15673c82))


### Tests

* **ansible-lint:** address multiple lint warnings ([fb9476a](https://bitbucket.org/baosystems/baosystems.tomcat/commits/fb9476a71eacbe17d32cbbc15e0c60a2fd66e39d)), closes [/ansible-lint.readthedocs.io/en/latest/default_rules.html#role-name-does-not-match-a-z-a-z0-9](https://bitbucket.org/baosystems//ansible-lint.readthedocs.io/en/latest/default_rules.html/issues/role-name-does-not-match-a-z-a-z0-9)

## [1.2.0](https://bitbucket.org/baosystems/baosystems.tomcat/compare/1.2.0%0D1.1.0#diff) (2020-09-23)


### Features

* **upstream:** bump Tomcat versions ([3fee707](https://bitbucket.org/baosystems/baosystems.tomcat/commits/3fee707753016b52c7755ee6337129a239c69586))


### CI

* run systemd without privileged ([728718a](https://bitbucket.org/baosystems/baosystems.tomcat/commits/728718af0af73f8ca467f3102485d2c4ca1a837c))
* **molecule:** fix variables ([493a58b](https://bitbucket.org/baosystems/baosystems.tomcat/commits/493a58b5d062e3fa4694668de45afd9eef8cecdf))
* **molecule:** official CentOS AMIs ([63c2702](https://bitbucket.org/baosystems/baosystems.tomcat/commits/63c2702a7f61cbe6f8b69417ab1d91b9cae3f601))
* **molecule:** use groups by operating system ([8c8fea4](https://bitbucket.org/baosystems/baosystems.tomcat/commits/8c8fea42633902ec49dbe6bafd250a226058a0d5))


### Others

* **molecule:** line breaks for yaml readability ([6099137](https://bitbucket.org/baosystems/baosystems.tomcat/commits/6099137498c8875dbc2436cb59c25c7122189c23))

## [1.1.0](https://bitbucket.org/baosystems/baosystems.tomcat/compare/1.1.0%0D1.0.3#diff) (2020-08-07)


### Features

* **server.xml:** add multiple options to template ([19d4785](https://bitbucket.org/baosystems/baosystems.tomcat/commits/19d478543720c916e5ff46cfe7e49f86d6ad7159))


### Docs

* **readme:** link to LICENSE file on Bitbucket web UI ([eb25dda](https://bitbucket.org/baosystems/baosystems.tomcat/commits/eb25dda45754296dd9c122515048216b69b50ff9))
* **readme:** minor improvements ([6cb0f07](https://bitbucket.org/baosystems/baosystems.tomcat/commits/6cb0f073207b25327c8a2db35e3490fc01f7e6f6))


### Others

* fix comment typos ([94384e1](https://bitbucket.org/baosystems/baosystems.tomcat/commits/94384e13e61743fa62644846094f765f6924fcf2))
* move "important" defaults to the top of the file ([c85c918](https://bitbucket.org/baosystems/baosystems.tomcat/commits/c85c918e49cc8fee08ed98a1197ebc0b02ba8b33))


### Tests

* separate unit test files for default settings ([93b809b](https://bitbucket.org/baosystems/baosystems.tomcat/commits/93b809b5809d46bbdfdeffdefdd7e5de94bae5ee))
* **server.xml:** verify scheme="https" and secure="true" ([ee56c4b](https://bitbucket.org/baosystems/baosystems.tomcat/commits/ee56c4b6d0cbb396fd96024146e11c4f276b91d2))

## [1.0.3](https://bitbucket.org/baosystems/baosystems.tomcat/compare/1.0.3%0D1.0.2#diff) (2020-07-21)


### Bug Fixes

* **upstream:** bump Tomcat 7 and Tomcat 9 source patch versions ([6ec698c](https://bitbucket.org/baosystems/baosystems.tomcat/commits/6ec698c9f8999932b5cdc1a3a5991ef371219e8c))
* **upstream:** bump Tomcat 8 to 8.5.57 ([a9bd232](https://bitbucket.org/baosystems/baosystems.tomcat/commits/a9bd232ab653c513546269b2f49fb46170d8623e))


### CI

* **pipelines:** bump image python:3.7-slim to python:3.8-slim ([eee4e1e](https://bitbucket.org/baosystems/baosystems.tomcat/commits/eee4e1e54604b278f1d4ebf1e04bbb786b667d8d))


### Others

* **pipenv:** remove pytnon 3.7 requirement ([5095501](https://bitbucket.org/baosystems/baosystems.tomcat/commits/5095501989fa44c173ed955fe8938ea139f4f315))

## [1.0.2](https://bitbucket.org/baosystems/baosystems.tomcat/compare/1.0.2%0D1.0.1#diff) (2020-07-10)


### Bug Fixes

* add 'secure="true"' alongside 'scheme="https"' ([4dda79e](https://bitbucket.org/baosystems/baosystems.tomcat/commits/4dda79ee8e2851281c932111aa04c4424d0b78f8))


### Tests

* remove unused scenarios ([590c1d8](https://bitbucket.org/baosystems/baosystems.tomcat/commits/590c1d8802d8e028d0ca53f5bb5836ac57956d10))


### CI

* combine CentOS 7 and 8 source scenarios ([6abe871](https://bitbucket.org/baosystems/baosystems.tomcat/commits/6abe87119767ae96acb6eea4c11972c37189f870))

## [1.0.1](https://bitbucket.org/baosystems/baosystems.tomcat/compare/1.0.1%0D1.0.0#diff) (2020-07-09)


### Reverts

* 8280008c ([8d62c75](https://bitbucket.org/baosystems/baosystems.tomcat/commits/8d62c754da34e47598212edebc03a5ffc812dbb8))


### Styling

* "do not edit" comments ([8280008](https://bitbucket.org/baosystems/baosystems.tomcat/commits/8280008cf15254dd23e2dfcfc99fbd8943290df7))


### Docs

* **development:** ensure files are readable by all users ([1cad46b](https://bitbucket.org/baosystems/baosystems.tomcat/commits/1cad46b6549dcb1eb5fe5a20129f7fa42b499b7d))
* **license:** typofix ([2c351d2](https://bitbucket.org/baosystems/baosystems.tomcat/commits/2c351d2f52632e7d68334f6ec2dfc65c22098606))
* **readme:** remove example text, bump example version ([59897cf](https://bitbucket.org/baosystems/baosystems.tomcat/commits/59897cfc1243fd4a65d3432d8da39439cf56ed5a))

## [1.0.0](https://bitbucket.org/baosystems/baosystems.tomcat/compare/1.0.0%0D0.4.3#diff) (2020-06-19)


### Features

* bump tomcat 8 to 8.5.56 ([4a7e8b5](https://bitbucket.org/baosystems/baosystems.tomcat/commits/4a7e8b5217bb370d080989a5788ee333059746f0))


### Bug Fixes

* **direnv:** add missing or ([f4094c5](https://bitbucket.org/baosystems/baosystems.tomcat/commits/f4094c559bfdb9e69b1c7201f919e82e48220779))


### Build System

* **npm:** update node_modules ([edaa185](https://bitbucket.org/baosystems/baosystems.tomcat/commits/edaa185a5ade3bad12c75457393d8ad654a9165f))
* **pipenv:** update virtualenv ([eb053c1](https://bitbucket.org/baosystems/baosystems.tomcat/commits/eb053c1a208db74fd792daa19a5b3731a9e0b7ea))


### CI

* **pipelines:** use latest version of 'atlassian/bitbucket-upload-file' ([30a2370](https://bitbucket.org/baosystems/baosystems.tomcat/commits/30a2370582c652ee280eeb5290991f1190989062))
* combine ec2 x86 and arm scenarios ([6c3f9eb](https://bitbucket.org/baosystems/baosystems.tomcat/commits/6c3f9ebef031e6fd93796d3aff33a5c48b9d1264))
* switch availability zone ([f0eee2a](https://bitbucket.org/baosystems/baosystems.tomcat/commits/f0eee2a068bff63e987c3123853be254c7dd865d))


### Tests

* remove unused scenarios ([c9aba97](https://bitbucket.org/baosystems/baosystems.tomcat/commits/c9aba976699a4fd2e1066ad03c7360b59be93c1f))
* **podman:** add podman tests like docker but without privileged mode ([041917f](https://bitbucket.org/baosystems/baosystems.tomcat/commits/041917f66b85c1a3b38b49a700377fd0cff3275c))


### Others

* **license:** update header ([f2f24d3](https://bitbucket.org/baosystems/baosystems.tomcat/commits/f2f24d38783eaa0888e03b4cde123717ced92af8))
* add auto_silent for python ([46eee03](https://bitbucket.org/baosystems/baosystems.tomcat/commits/46eee032d885907b67eb50c0e5b3b4220769d884))

## [0.4.3](https://bitbucket.org/baosystems/baosystems.tomcat/compare/0.4.3%0D0.4.2#diff) (2020-05-27)


### Features

* bump tomcat_version to 8.5.55 ([fdb91f2](https://bitbucket.org/baosystems/baosystems.tomcat/commits/fdb91f20c5539b75cb95d045815f3328e1dce533))


### CI

* **ec2:** default instance type from a1.medium to m6g.medium ([bf7c04d](https://bitbucket.org/baosystems/baosystems.tomcat/commits/bf7c04d2e695c5dd51eba604bc5bba7ba25db80b))
* **ec2:** scenarios to test on ARM in EC2 ([4638d14](https://bitbucket.org/baosystems/baosystems.tomcat/commits/4638d140cb37190747fa362c6eee96960deba3da))


### Tests

* **ec2:** wait changed to ssh success ([e40d0c7](https://bitbucket.org/baosystems/baosystems.tomcat/commits/e40d0c7e6704068559825a40b150618300d9b557))

## [0.4.2](https://bitbucket.org/baosystems/baosystems.tomcat/compare/0.4.2%0D0.4.1#diff) (2020-05-13)


### Bug Fixes

* keep an existing uid/gid for tomcat ([c6a4dc5](https://bitbucket.org/baosystems/baosystems.tomcat/commits/c6a4dc55653781345133a131280670b128ef3c50))
* remove shutdown port ([12f596c](https://bitbucket.org/baosystems/baosystems.tomcat/commits/12f596c72a1dc47fb7d1598f138bcfb7c8f0bc1a))


### Docs

* **readme:** update dependency URL ([16bde4c](https://bitbucket.org/baosystems/baosystems.tomcat/commits/16bde4c64399d1e7491801c02028c86e9f122115))

## [0.4.1](https://bitbucket.org/baosystems/baosystems.tomcat/compare/0.4.1%0D0.4.0#diff) (2020-05-12)


### Docs

* **readme:** minor update ([98bb97c](https://bitbucket.org/baosystems/baosystems.tomcat/commits/98bb97cddb9d115e7cde3382eb4552e402f87c9a))


### Others

* **pipelines:** clean up bitbucket-pipelines.yml ([e67a7bb](https://bitbucket.org/baosystems/baosystems.tomcat/commits/e67a7bb70bde2b192810bda4f3b01766fda1a0db))
* add commented-out sample option ([bad7a9d](https://bitbucket.org/baosystems/baosystems.tomcat/commits/bad7a9d22cffa80eaddb7de373feee623c4020c8))
* remove unused variable and logic ([23e22e7](https://bitbucket.org/baosystems/baosystems.tomcat/commits/23e22e7691ee893242536a709f213e9a3307d515))
* speed up side-effect ([83cb4ae](https://bitbucket.org/baosystems/baosystems.tomcat/commits/83cb4ae637b74899356015f25973173aa39594a9))


### Code Refactoring

* tasks from variables.yml moved ([a3ce7dc](https://bitbucket.org/baosystems/baosystems.tomcat/commits/a3ce7dc96b2ca7a11a7922acf55a83e91d4275f3))


### CI

* **pipelines:** archives for branch ([86b0ae9](https://bitbucket.org/baosystems/baosystems.tomcat/commits/86b0ae9e19049fbb251852b45e8787eafe0c388d))
* **pipelines:** increase clone depth ([d16a6e4](https://bitbucket.org/baosystems/baosystems.tomcat/commits/d16a6e4b94cc5b4dbdbcb2ac0b8410421ea82a74))


### Tests

* re-organize test files ([913432c](https://bitbucket.org/baosystems/baosystems.tomcat/commits/913432c28877d784c3d4c6ac77666d2658285040))

## [0.4.0](https://bitbucket.org/baosystems/baosystems.tomcat/compare/0.4.0%0D0.3.0#diff) (2020-05-11)


### ⚠ BREAKING CHANGES

* improve switching between installation methods

### Features

* improve switching between installation methods ([5786564](https://bitbucket.org/baosystems/baosystems.tomcat/commits/5786564b6ffa5ef7e64c03f5ac884fd8994d097d))
* run geerlingguy.java as dependency to simplify ([02556d0](https://bitbucket.org/baosystems/baosystems.tomcat/commits/02556d0f7f9e226b7a69ba413b2b6ada12650edb))


### Bug Fixes

* include hidden files when emptying ([b626e16](https://bitbucket.org/baosystems/baosystems.tomcat/commits/b626e16f215edfb5d91a872a9f5914706855baab))
* remove reference to apache-commons-daemon jar ([3c539c8](https://bitbucket.org/baosystems/baosystems.tomcat/commits/3c539c8839a9a37d4728c26d4de393b301ac9546))


### CI

* **pipelines:** create archives as Pipelines steps ([ee1afe5](https://bitbucket.org/baosystems/baosystems.tomcat/commits/ee1afe5792f836077f7f3d1494e4449d3184bee8))


### Build System

* **galaxy:** geerlingguy.java 1.10.0 ([0165743](https://bitbucket.org/baosystems/baosystems.tomcat/commits/016574366d129edfd4a53041f29d55657b14c71b))


### Tests

* simplify test ([561d65b](https://bitbucket.org/baosystems/baosystems.tomcat/commits/561d65b234f0394d7b533827442ac632b6637bbc))
* use group_vars.all instead of host_vars.$instance ([bdae672](https://bitbucket.org/baosystems/baosystems.tomcat/commits/bdae67273328e025b55d78beb1cb0b478c0839e0))


### Others

* simplify meta/main.yml and readme ([deca560](https://bitbucket.org/baosystems/baosystems.tomcat/commits/deca560e5cad0a3cbd2d0bf6f7c8cb77227f3fe9))


### Docs

* **development:** how to release a version ([af38d61](https://bitbucket.org/baosystems/baosystems.tomcat/commits/af38d61a2b2b37abf84790c4ada16fce6f7a0bca))
* **license:** add LICENSE file for MIT license ([663cccc](https://bitbucket.org/baosystems/baosystems.tomcat/commits/663cccc00e91e1eb0f65580f7cf74ac2d113beb3))


### Code Refactoring

* combine setup/requirement tasks ([766c4a0](https://bitbucket.org/baosystems/baosystems.tomcat/commits/766c4a07d39945b05d6efa12b9fde645614a8a78))

## [0.3.0](https://bitbucket.org/baosystems/baosystems.tomcat/compare/0.3.0%0D0.2.3#diff) (2020-05-07)


### Build System

* **pipenv:** update virtualenv ([0469f7b](https://bitbucket.org/baosystems/baosystems.tomcat/commits/0469f7bff5930f82c57d603d0b0b89b959ee9784))


### CI

* change environment variables ([741d5a1](https://bitbucket.org/baosystems/baosystems.tomcat/commits/741d5a12870f44f5dbd37b6dcff2ad7a0816a8e0))
* publish releases as zip archives ([995dd76](https://bitbucket.org/baosystems/baosystems.tomcat/commits/995dd762777291f403fc7bfc13e371444e601bfe))

## [0.2.3](https://bitbucket.org/baosystems/baosystems.tomcat/compare/0.2.3%0D0.2.2#diff) (2020-05-07)


### Features

* support for tomcat_connector_relaxedquerychars ([91e5a4d](https://bitbucket.org/baosystems/baosystems.tomcat/commits/91e5a4dcf306ae99941dd7368c6223961bef0e42))


### Bug Fixes

* backup all config files ([5c44af0](https://bitbucket.org/baosystems/baosystems.tomcat/commits/5c44af0ae7a141e0411b728189285ea98629accf))


### Docs

* **readme:** improve notes, sort options ([8bb1151](https://bitbucket.org/baosystems/baosystems.tomcat/commits/8bb1151cc70f364fdb34ff657397c4b37fb97053))

## [0.2.2](https://bitbucket.org/baosystems/baosystems.tomcat/compare/0.2.2%0D0.2.1#diff) (2020-05-06)


### Features

* handler "remove previous sources" ([cf48870](https://bitbucket.org/baosystems/baosystems.tomcat/commits/cf48870ae614b9eae75593006a8898845497be75))


### Others

* remove unnecessary "become" ([0c3812e](https://bitbucket.org/baosystems/baosystems.tomcat/commits/0c3812ec106462203790b5900c7e1e8c6ce38cc5))
* split up into two tasks to have bin and lib separate ([07a1802](https://bitbucket.org/baosystems/baosystems.tomcat/commits/07a180238e011a21f57f657f38adbbe88b879860))
* update comments ([c033968](https://bitbucket.org/baosystems/baosystems.tomcat/commits/c033968a6ef376d82eef9b27382820a5a7f295dc))

## [0.2.1](https://bitbucket.org/baosystems/baosystems.tomcat/compare/0.2.1%0D0.2.0#diff) (2020-05-05)


### Features

* ensure tomcat started at completion ([8cc7d14](https://bitbucket.org/baosystems/baosystems.tomcat/commits/8cc7d148d378923c2769fb5b0ea91b18f0e492c7))
* files and templates for tomcat 7 package ([6f7b327](https://bitbucket.org/baosystems/baosystems.tomcat/commits/6f7b32793732467166963d3151c1baf67901ebcc))
* set up tomcat_install_method=packages ([0066957](https://bitbucket.org/baosystems/baosystems.tomcat/commits/0066957a32c604cc2dcb21268161dd67ad4a4658))
* support for Xms and Xms values ([81c43b2](https://bitbucket.org/baosystems/baosystems.tomcat/commits/81c43b241459d04e7b08f1ead6210ac5d17ae8d6))
* tomcat_parameter_xms and tomcat_parameter_xmx not set ([358e690](https://bitbucket.org/baosystems/baosystems.tomcat/commits/358e6907bb506237d1ff657cdde20af6d0497fce))


### Bug Fixes

* add "restart tomcat" handler and use more ([7a14b37](https://bitbucket.org/baosystems/baosystems.tomcat/commits/7a14b376a0c5b7716e025c6c6740cabd7118a5a6))
* backup config files changes ([1b9b758](https://bitbucket.org/baosystems/baosystems.tomcat/commits/1b9b7580bbe0d0a12c3969c4ce117cb3948296b1))
* flip backup from true to false ([7815fae](https://bitbucket.org/baosystems/baosystems.tomcat/commits/7815fae27d5cf13830f4f206c2baa498c37d6f53))
* install_packages can remove sample-webapps ([6095cb3](https://bitbucket.org/baosystems/baosystems.tomcat/commits/6095cb376ed27adb3e20287b8bc67875ec9ebe2a))
* missed versioning path ([8193824](https://bitbucket.org/baosystems/baosystems.tomcat/commits/81938249dde9aefa9e1c95d107f6bfa0876141da))
* **gc.conf:** change from template to file ([9534b5a](https://bitbucket.org/baosystems/baosystems.tomcat/commits/9534b5af655e0b38e11a38b6c571f19475650a92))


### Others

* add comments for changing options ([b6f9269](https://bitbucket.org/baosystems/baosystems.tomcat/commits/b6f92695aaf327411c6ded43e2a8f2d963038f1c))
* better test, explain parameter ([3dd8c1c](https://bitbucket.org/baosystems/baosystems.tomcat/commits/3dd8c1ca50c77c91a103bdf4017450f353665603))
* clarify comment ([54edcf9](https://bitbucket.org/baosystems/baosystems.tomcat/commits/54edcf945a6a7454926f135ce2a3ba040022bbb8))
* commented options as development toggles ([be73e11](https://bitbucket.org/baosystems/baosystems.tomcat/commits/be73e11e131879821fc0bb62f1566d752f87816c))
* compare int instead of string ([bb10b78](https://bitbucket.org/baosystems/baosystems.tomcat/commits/bb10b78f46b1ead3cbee11282c81bd3041214e35))
* easier to read pipeline step in web UI ([caa7357](https://bitbucket.org/baosystems/baosystems.tomcat/commits/caa73575dc2f56ed77d8a0b1185122d55f9dbdfb))
* **meta:** list CentOS 8 ([87bd515](https://bitbucket.org/baosystems/baosystems.tomcat/commits/87bd515f8f8f1d800717bc77cab1f2299da231b2))
* group related actions ([d068397](https://bitbucket.org/baosystems/baosystems.tomcat/commits/d0683973f871558d13fe906e3363227caf615cec))
* rename instances for readability ([57d6726](https://bitbucket.org/baosystems/baosystems.tomcat/commits/57d67266475419fe4d344f136dd65850231a9b84))
* specify templates/ or files/ for readability ([c3f034b](https://bitbucket.org/baosystems/baosystems.tomcat/commits/c3f034b267750ee32fb44e89edfee4b80d84af48))
* update comments ([e5d2167](https://bitbucket.org/baosystems/baosystems.tomcat/commits/e5d216730e77f6a22fcac6cb4b6838677b9d0bb3))
* update comments and notes ([ca8a067](https://bitbucket.org/baosystems/baosystems.tomcat/commits/ca8a06753949a19e07a63debce4f3139634070b2))


### Tests

* **molecule:** refactor molecule scenarios to group install methods ([1e03f6e](https://bitbucket.org/baosystems/baosystems.tomcat/commits/1e03f6ebce9d441d0fd8b4c4b19efd566da0dfde))
* add tests for tasks/requirements.yml ([507fbbb](https://bitbucket.org/baosystems/baosystems.tomcat/commits/507fbbbd2945f1ae975d07fd183e21a2dfa4297a))
* fix paths to include source tests ([0fb5909](https://bitbucket.org/baosystems/baosystems.tomcat/commits/0fb5909726161eac3dda1b6b964b633dba844b51))
* split out centos 8 ([7c0b65c](https://bitbucket.org/baosystems/baosystems.tomcat/commits/7c0b65cb81684e6773f3fc47e25aa304188fdf9f))


### Styling

* **server.xml:** whitespace for readability ([02bbbdd](https://bitbucket.org/baosystems/baosystems.tomcat/commits/02bbbddc021df574efaf02a256fdb86a75140915))


### Docs

* **changelog:** remove breaking changes note ([c1c7394](https://bitbucket.org/baosystems/baosystems.tomcat/commits/c1c7394139f65ab018e8174415be24d6fa5be335))
* **readme:** initial README ([c74104c](https://bitbucket.org/baosystems/baosystems.tomcat/commits/c74104c9f6de46f5fec539d4b688f8ea3542fe72))
* **readme:** types for parameters ([e542514](https://bitbucket.org/baosystems/baosystems.tomcat/commits/e5425146e100e1bbcd948b8a49856ca4ebef4ce5))
* usage and development docs ([4db4df0](https://bitbucket.org/baosystems/baosystems.tomcat/commits/4db4df0eb0dbf03b706edbe551f76b3262822c17))


### Code Refactoring

* **files:** move to source subdirectory ([3a9649a](https://bitbucket.org/baosystems/baosystems.tomcat/commits/3a9649af720aa037e2b660c7c88a3c4fdef1994f))
* **tasks:** use "become" once ([476da9f](https://bitbucket.org/baosystems/baosystems.tomcat/commits/476da9f29726c8370dddf4466fa9c87900e5a586))
* move test for source method out of default folder ([816ed14](https://bitbucket.org/baosystems/baosystems.tomcat/commits/816ed14dea8d69fc3dfae26109a631432edf8efc))
* move tomcat_version_major to each task ([1b626af](https://bitbucket.org/baosystems/baosystems.tomcat/commits/1b626afe3d28374ed7e3898f4e0ffd27f84c4242))
* shuffle files around for versioning ([8398490](https://bitbucket.org/baosystems/baosystems.tomcat/commits/8398490b8e2c01abb17cb23292e012d2d716e7ff))
* simplify installation, files, templates, and tests ([cc40b57](https://bitbucket.org/baosystems/baosystems.tomcat/commits/cc40b57346c1c7319c7886f7685033fee0178c30))

## [0.2.0](https://bitbucket.org/baosystems/baosystems.tomcat/compare/0.2.0%0D0.1.2#diff) (2020-04-22)


### Features

* **user:** tomcat user, group, and homedir ([05ff31c](https://bitbucket.org/baosystems/baosystems.tomcat/commits/05ff31c58ff30d1d4275e10dde9b852c102ffe21))
* install tomcat from source ([0acc279](https://bitbucket.org/baosystems/baosystems.tomcat/commits/0acc279cd27654057c92caadf38c6bc8f8af9a3b))


### Styling

* add ".yml" to name fields ([0470866](https://bitbucket.org/baosystems/baosystems.tomcat/commits/047086632f808c4397dd0d2e91791edf8de9e749))
* comments and names ([c895e7d](https://bitbucket.org/baosystems/baosystems.tomcat/commits/c895e7d891f449137c09143dd3a0e9630ac599a1))


### Tests

* **testinfra:** rename file to match the task file ([38fca73](https://bitbucket.org/baosystems/baosystems.tomcat/commits/38fca7356d7b53834a970ea2aa2ee83638a4abc1))
* centos7 as centos8 separate scenarios ([0816d3f](https://bitbucket.org/baosystems/baosystems.tomcat/commits/0816d3fb3282e16ec02244aa5fae636ee72f30f4))


### Others

* **direnv:** tags with git pull and push ([40572f6](https://bitbucket.org/baosystems/baosystems.tomcat/commits/40572f694051645deec941dcae3a4bb3966d4cf0))
* remove unused files ([acac632](https://bitbucket.org/baosystems/baosystems.tomcat/commits/acac63262b038751d4867885703bc48c9f02ae36))


### CI

* **pipelines:** less output from apt ([c30cc80](https://bitbucket.org/baosystems/baosystems.tomcat/commits/c30cc80d0a37dc8632d3e4d3463d15fdce4fc7b8))
* **pipelines:** use a shallow clone ([d24edde](https://bitbucket.org/baosystems/baosystems.tomcat/commits/d24edded4044fc171e968b4d0f0c7222377c1fa7))
* **pipelines:** use oauth instead of app password ([8df5f8f](https://bitbucket.org/baosystems/baosystems.tomcat/commits/8df5f8f89b130c6b77ac304faaf41db5fc1c27f7))

## [0.1.2](https://bitbucket.org/baosystems/baosystems.tomcat/compare/0.1.2%0D0.1.1#diff) (2020-04-10)


### Features

* **packages:** install java ([0dfc4e8](https://bitbucket.org/baosystems/baosystems.tomcat/commits/0dfc4e85603e555792e7f74bdda8dfb06d137c3e))


### Others

* **commitizen:** set up commitizen ([4ba3f96](https://bitbucket.org/baosystems/baosystems.tomcat/commits/4ba3f96a4a2831b0853a09706eb22c04b103c187))
* **direnv:** check for pipenv ([a20653b](https://bitbucket.org/baosystems/baosystems.tomcat/commits/a20653bce55183124186048ed1f6828a1fa9fd16))
* **direnv:** load pipenv earlier ([e1a825b](https://bitbucket.org/baosystems/baosystems.tomcat/commits/e1a825bc80345c2bb6690b1893f9278bdf45d6a8))
* fix line endings on multiple files ([ae323cd](https://bitbucket.org/baosystems/baosystems.tomcat/commits/ae323cd30feee94f7250b22ae4768b5e975e10e7))
* **molecule:** add flake8 linter ([4f947aa](https://bitbucket.org/baosystems/baosystems.tomcat/commits/4f947aa5372d101b1faee9b2b982abe60d93f757))
* **molecule:** run testinfra with sudo ([179d281](https://bitbucket.org/baosystems/baosystems.tomcat/commits/179d28180490e00742bbb8db5bf81a5617ad8aec))
* **molecule:** share requirements with default ([fff164e](https://bitbucket.org/baosystems/baosystems.tomcat/commits/fff164efbfe08d0a88a8028414845b8c05bf892e))
* **molecule:** share tests for multiple scenarios ([c7fc842](https://bitbucket.org/baosystems/baosystems.tomcat/commits/c7fc842905412559026d6192092df06583e2c34e))
* **molecule:** test centos8 at the same time ([883803f](https://bitbucket.org/baosystems/baosystems.tomcat/commits/883803fbc6ecab79c61453926411a5ebdec98cf6))
* **molecule:** test to see if we can work w/o the delay ([981f8ad](https://bitbucket.org/baosystems/baosystems.tomcat/commits/981f8ad4f73cfae9c48af7a49b2e658938151aa7))
* **pre-commit:** initial .pre-commit-config.yaml file ([73fc72c](https://bitbucket.org/baosystems/baosystems.tomcat/commits/73fc72c079d085939bc25859ed2e3c76a084dede))
* **pre-commit:** set up commitlint ([917283d](https://bitbucket.org/baosystems/baosystems.tomcat/commits/917283d7d5b66e1763820a843a50622c3572b670))

## [0.1.1](https://bitbucket.org/baosystems/baosystems.tomcat/compare/0.1.1%0D0.1.0#diff) (2020-04-09)


### Features

* **molecule:** add EC2 scenario ([42ef8ec](https://bitbucket.org/baosystems/baosystems.tomcat/commits/42ef8ec4f5180518711cb4ffc5f2149588f400d3))
* **molecule:** enable ansible-lint and yamllint ([6d89c93](https://bitbucket.org/baosystems/baosystems.tomcat/commits/6d89c9373aca72ff93228f050e791b225ccb3fe0))
* **standard-version:** initial .versionrc.json ([624446c](https://bitbucket.org/baosystems/baosystems.tomcat/commits/624446cb476d78d1ae764dd4c01c7e5e2b7d4163))


### Bug Fixes

* **ansible-lint:** resolve or ignore errors ([e8dd7d5](https://bitbucket.org/baosystems/baosystems.tomcat/commits/e8dd7d52d26fc72700d4afcda250a77743c0d830))
* **molecule:** improvements to EC2 create.yml... ([5ba2d10](https://bitbucket.org/baosystems/baosystems.tomcat/commits/5ba2d10718b433f627d8e7876915ce2f6952b61d))
* **molecule:** resolve Ansible deprecation notice ([7563e14](https://bitbucket.org/baosystems/baosystems.tomcat/commits/7563e1458e0173e1617cde1f493532cf24161795))
* **molecule:** set image, instance_type, and vpc_subnet_id for EC2 ([48d3907](https://bitbucket.org/baosystems/baosystems.tomcat/commits/48d390780e13c74cc5ff6670de1798d0e1ae63a4))
* **molecule:** share converge.yml with the default scenario ([9acceb3](https://bitbucket.org/baosystems/baosystems.tomcat/commits/9acceb3488a59383b16328b060ecfcb769fd408a))
* **yamllint:** ignore .venv/ ([1215c43](https://bitbucket.org/baosystems/baosystems.tomcat/commits/1215c433acdee82dc00c785228d69b5e87268980))


### CI

* **pipelines:** initial bitbucket-pipelines.yml file ([792a027](https://bitbucket.org/baosystems/baosystems.tomcat/commits/792a027b44b5aaef1ed238387edd1b6e38b179fe))
