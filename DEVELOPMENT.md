# Upgrading Tomcat

See if an update is available by visiting https://archive.apache.org/dist/tomcat/tomcat-8/?C=M;O=D

Scenario: Tomcat is updated from 8.5.98 from 8.5.99:

1. Edit files:

    * defaults/main.yml:

        * tomcat_version default value and the comment above it

    * molecule/shared/tests/test_install_source_8.py:

        * replace string "8.5.98" with "8.5.99"

        * update the sha256sum in test_archive_extracted(): `curl -s https://archive.apache.org/dist/tomcat/tomcat-8/v8.5.99/bin/apache-tomcat-8.5.99.tar.gz | tar -f - -z -x --to-stdout apache-tomcat-8.5.99/RELEASE-NOTES | sha256sum`

    * README.md:

        * replace strings "8.5.98" with "8.5.99"

    * DEVELOPMENT.md:

        * Run through the steps that would extract files from apache-tomcat-8.5.99.tar.gz and see if
          the various conf files changed, as shown below (via `git status`)

        * If no major problems: replace string "8.5.98" with "8.5.99"

        * If any files have changed, you may need to update checksums in molecule/shared/tests/.

2. Lint and run all tests locally:

    ```bash
    molecule lint

    molecule test --scenario-name source_8
    ```

3. If desired, run test(s) on EC2 instance in AWS account #956183978115 ("bao-sandbox"):

    ```bash
    aws2-wrap --profile sandbox molecule test --scenario-name ec2_source_8
    ```


# CI Releases

The _bitbucket-pipelines.yml_ file will run `standard-version` on the master branch. To create a release, create a pull request from develop to master. The most-recent develop commit should have a passing CI test. When you merge the PR, Pipelines will run and create the release. It is dependent on the branch being master, so you cannot run the pipeline ad-hoc on the commit later. This means that the most-recent commit that is merged into the master branch cannot contain _[skip ci]_.


# Directory Layout

The `defaults`, `files`, `handlers`, `meta`, `tasks`, `templates`, and `vars` directories comprise the Ansible role. The `molecule` directory is for Molecule.


## _files_ and _templates_ directories

In an Ansible role, there are _files_ and _templates_. Templates typically contain a value or variable that would be changed by a dynamic value, while a File is copied to the destination without being changed. A template usually has the _.j2_ extension to signify it is a Jinja2 template.

The following explains the contents of the `files/` and `templates/` directories:

* `packages` - Used only by the _packages_ installation method.
* `source` - Used only by the _source_ installation method.
* `common` - Applicable to both installation methods _packages_ and _source_.

Each of the three directories have additional subdirectories `7`, `8`, and/or `9`, and are applicable only for the major Tomcat version. The `all` directory is applicable for all major versions of Tomcat.


## _molecule_ directory

With the exception of the _shared_ directory, the directories are the names of the Molecule scenarios:

* `default` - Uses Docker to run the _packages_ installation method on **CentOS 7**. Runs when you do not specify a scenario, such as `molecule test`. Also contains linting commands (no other scenarios will perform linting commands).
* `source_8` - Uses Docker to run the _source_ installation method on **CentOS 7**.
* `ec2` - Uses AWS EC2 to run the _packages_ installation method on **CentOS 7**. Requires AWS API keys to be present in the shell environment. This scenario is run by Bitbucket Pipelines and uses API keys saved in the project Pipeline settings.
* `ec2_source_8` - Uses AWS EC2 to run the _source_ installation method on **CentOS 7**. Requires AWS API keys to be present in the shell environment. This scenario is run by Bitbucket Pipelines and uses API keys saved in the project Pipeline settings.

The _molecule/shared_ directory is not a scenario. Its contents are as follows:

* `shared/common` - Shared scenario sequence playbooks and tests. Applicable to any installation method and major Tomcat version.
* `shared/ec2` - Shared scenario sequence playbooks for the EC2 driver.
* `shared/tests` - Shared tests for all scenarios and installation methods. Intended to load individual test files rather than the entire directory.


## _vars_ directory

There is no _vars/default.yml_ file. The contents of _vars_ are files specifically for each supported operating system. The file _tasks/variables.yml_ contains the Ansible command that will load the applicable file on a target.


# Upstream files

A lot of the files in the `files/` directory were downloaded from either the Apache Tomcat distribution archives or the CentOS/Fedora RPM sources. This is because the _source_ installation method is mimicking the layout of the _packages_ method. The following commands were used to download the initial versions. A note about the files being managed by Ansible is added to each file where possible.

The following shell commands, run from the base directory of this role:

```bash
# https://git.centos.org/rpms/tomcat/blob/c7/f/SOURCES

pushd files/source/7

mkdir -pv opt/tomcat/libexec/
curl -o opt/tomcat/libexec/functions https://git.centos.org/rpms/tomcat/raw/c7/f/SOURCES/tomcat-functions
curl -o opt/tomcat/libexec/preamble https://git.centos.org/rpms/tomcat/raw/c7/f/SOURCES/tomcat-preamble
curl -o opt/tomcat/libexec/server https://git.centos.org/rpms/tomcat/raw/c7/f/SOURCES/tomcat-server
chmod -c 0644 opt/tomcat/libexec/functions
chmod -c 0755 opt/tomcat/libexec/{preamble,server}
sed -e 's,/usr/libexec/tomcat,/opt/tomcat/libexec,g' -e '3i ## DO NOT EDIT\n## File managed by Ansible.\n\n' -i opt/tomcat/libexec/functions
# Also remove reference to apache-commons-daemon jar
sed -e 's,/usr/libexec/tomcat,/opt/tomcat/libexec,g' -e '3i ## DO NOT EDIT\n## File managed by Ansible.\n\n' -e '/build-classpath commons-daemon/d' -i opt/tomcat/libexec/preamble
sed -e 's,/usr/libexec/tomcat,/opt/tomcat/libexec,g' -e '3i ## DO NOT EDIT\n## File managed by Ansible.\n\n' -i opt/tomcat/libexec/server

mkdir -pv etc/logrotate.d/
curl -o etc/logrotate.d/tomcat https://git.centos.org/rpms/tomcat/raw/c7/f/SOURCES/tomcat-7.0.logrotate
sed -e 's,@@@TCLOG@@@,/var/log/tomcat,g' -e '1i ## DO NOT EDIT\n## File managed by Ansible.\n\n' -i etc/logrotate.d/tomcat

mkdir -pv etc/systemd/system
curl -o etc/systemd/system/tomcat.service https://git.centos.org/rpms/tomcat/raw/c7/f/SOURCES/tomcat-7.0.service
curl -o etc/systemd/system/tomcat\@.service https://git.centos.org/rpms/tomcat/raw/c7/f/SOURCES/tomcat-named.service
sed -e 's,/usr/libexec/tomcat,/opt/tomcat/libexec,g' -e '1i ## DO NOT EDIT\n## File managed by Ansible.\n\n' -i etc/systemd/system/tomcat.service
sed -e 's,/usr/libexec/tomcat,/opt/tomcat/libexec,g' -e '1i ## DO NOT EDIT\n## File managed by Ansible.\n\n' -i etc/systemd/system/tomcat\@.service

# http://mirror.centos.org/centos/7/os/x86_64/Packages/

mkdir -pv etc/sysconfig
rpm2cpio <( curl -fsSL "http://mirror.centos.org/centos/7/os/x86_64/Packages/tomcat-7.0.76-15.el7.noarch.rpm" ) | cpio --quiet --extract --to-stdout ./etc/sysconfig/tomcat > etc/sysconfig/tomcat
sed -e '1i ## DO NOT EDIT\n## File managed by Ansible.\n\n' -e '$ a \\n\n##\n## DO NOT EDIT\n##\n## Set variables in files in /etc/tomcat/conf.d/\n##' -i etc/sysconfig/tomcat

mkdir -pv etc/tomcat
rpm2cpio <( curl -fsSL "http://mirror.centos.org/centos/7/os/x86_64/Packages/tomcat-7.0.76-15.el7.noarch.rpm" ) | cpio --quiet --extract --to-stdout ./etc/tomcat/tomcat.conf > etc/tomcat/tomcat.conf
# Also remove option to use a library from the RPM that we don't have
sed \
  -e '1i ## DO NOT EDIT\n## File managed by Ansible.\n\n' \
  -e '$ a \\n\n##\n## DO NOT EDIT\n##\n## Set variables in files in /etc/tomcat/conf.d/\n##' \
  -e '/^$/{N;/^\n$/d;}' \
  -e '/javax.sql.DataSource/d' \
  -i etc/tomcat/tomcat.conf

# https://archive.apache.org/dist/tomcat/tomcat-7/?C=M;O=D

mkdir -pv etc/tomcat
curl https://archive.apache.org/dist/tomcat/tomcat-7/v7.0.109/bin/apache-tomcat-7.0.109.tar.gz | tar -f - -z -x -v -C etc/tomcat --strip 2 apache-tomcat-7.0.109/conf
sed -e '1i // DO NOT EDIT\n// File managed by Ansible.\n\n' -i etc/tomcat/catalina.policy
sed -e '1i ## DO NOT EDIT\n## File managed by Ansible.\n\n' -i etc/tomcat/catalina.properties
sed -e '2i \\n\n<!-- ## DO NOT EDIT              -->\n<!-- ## File managed by Ansible. -->\n\n' -i etc/tomcat/context.xml
sed -e '1i ## DO NOT EDIT\n## File managed by Ansible.\n\n' -i etc/tomcat/logging.properties
sed -e '2i \\n\n<!-- ## DO NOT EDIT              -->\n<!-- ## File managed by Ansible. -->\n\n' -i etc/tomcat/tomcat-users.xml
sed -e '2i \\n\n<!-- ## DO NOT EDIT              -->\n<!-- ## File managed by Ansible. -->\n\n' -i etc/tomcat/tomcat-users.xsd
sed -e '2i \\n\n<!-- ## DO NOT EDIT              -->\n<!-- ## File managed by Ansible. -->\n\n' -i etc/tomcat/web.xml
# NOTE: We use a template for server.xml, so delete for now
rm -v etc/tomcat/server.xml
# This removes an extra blank line from the end for linting rules
sed -e '$d' -i etc/tomcat/catalina.policy
# These files need a newline at the end for linting rules
printf '\n' >> etc/tomcat/context.xml
printf '\n' >> etc/tomcat/tomcat-users.xsd

popd

################################################################

# https://src.fedoraproject.org/rpms/tomcat/tree/f28

pushd files/source/8

mkdir -pv opt/tomcat/libexec/
curl -o opt/tomcat/libexec/functions https://src.fedoraproject.org/rpms/tomcat/raw/f28/f/tomcat-functions
curl -o opt/tomcat/libexec/preamble https://src.fedoraproject.org/rpms/tomcat/raw/f28/f/tomcat-preamble
curl -o opt/tomcat/libexec/server https://src.fedoraproject.org/rpms/tomcat/raw/f28/f/tomcat-server
chmod -c 0644 opt/tomcat/libexec/functions
chmod -c 0755 opt/tomcat/libexec/{preamble,server}
sed -e 's,/usr/libexec/tomcat,/opt/tomcat/libexec,g' -e '3i ## DO NOT EDIT\n## File managed by Ansible.\n\n' -i opt/tomcat/libexec/functions
# Also remove reference to apache-commons-daemon jar
sed -e 's,/usr/libexec/tomcat,/opt/tomcat/libexec,g' -e '3i ## DO NOT EDIT\n## File managed by Ansible.\n\n' -e '/build-classpath commons-daemon/d' -i opt/tomcat/libexec/preamble
sed -e 's,/usr/libexec/tomcat,/opt/tomcat/libexec,g' -e '3i ## DO NOT EDIT\n## File managed by Ansible.\n\n' -i opt/tomcat/libexec/server

mkdir -pv etc/logrotate.d/
curl -o etc/logrotate.d/tomcat https://src.fedoraproject.org/rpms/tomcat/raw/f28/f/tomcat-8.5.logrotate
sed -e 's,@@@TCLOG@@@,/var/log/tomcat,g' -e '1i ## DO NOT EDIT\n## File managed by Ansible.\n\n' -i etc/logrotate.d/tomcat

mkdir -pv etc/systemd/system
curl -o etc/systemd/system/tomcat.service https://src.fedoraproject.org/rpms/tomcat/raw/f28/f/tomcat-8.5.service
curl -o etc/systemd/system/tomcat\@.service https://src.fedoraproject.org/rpms/tomcat/raw/f28/f/tomcat-named.service
sed -e 's,/usr/libexec/tomcat,/opt/tomcat/libexec,g' -e '1i ## DO NOT EDIT\n## File managed by Ansible.\n\n' -i etc/systemd/system/tomcat.service
sed -e 's,/usr/libexec/tomcat,/opt/tomcat/libexec,g' -e '1i ## DO NOT EDIT\n## File managed by Ansible.\n\n' -i etc/systemd/system/tomcat\@.service

# https://archives.fedoraproject.org/pub/archive/fedora/linux/releases/28/Everything/x86_64/os/Packages/t/

mkdir -pv etc/sysconfig
rpm2cpio <( curl -fsSL "https://archives.fedoraproject.org/pub/archive/fedora/linux/releases/28/Everything/x86_64/os/Packages/t/tomcat-8.5.29-1.fc28.noarch.rpm" ) | cpio --quiet --extract --to-stdout ./etc/sysconfig/tomcat > etc/sysconfig/tomcat
sed -e '1i ## DO NOT EDIT\n## File managed by Ansible.\n\n' -e '$ a \\n\n##\n## DO NOT EDIT\n##\n## Set variables in files in /etc/tomcat/conf.d/\n##' -i etc/sysconfig/tomcat

mkdir -pv etc/tomcat
rpm2cpio <( curl -fsSL "https://archives.fedoraproject.org/pub/archive/fedora/linux/releases/28/Everything/x86_64/os/Packages/t/tomcat-8.5.29-1.fc28.noarch.rpm" ) | cpio --quiet --extract --to-stdout ./etc/tomcat/tomcat.conf > etc/tomcat/tomcat.conf
# Also remove option to use a library from the RPM that we don't have
sed \
  -e '1i ## DO NOT EDIT\n## File managed by Ansible.\n\n' \
  -e '$ a \\n\n##\n## DO NOT EDIT\n##\n## Set variables in files in /etc/tomcat/conf.d/\n##' \
  -e '/^$/{N;/^\n$/d;}' \
  -e '/javax.sql.DataSource/d' \
  -i etc/tomcat/tomcat.conf

# https://archive.apache.org/dist/tomcat/tomcat-8/?C=M;O=D

mkdir -pv etc/tomcat
curl https://archive.apache.org/dist/tomcat/tomcat-8/v8.5.99/bin/apache-tomcat-8.5.99.tar.gz | tar -f - -z -x -v -C etc/tomcat --strip 2 apache-tomcat-8.5.99/conf
sed -e '1i // DO NOT EDIT\n// File managed by Ansible.\n\n' -i etc/tomcat/catalina.policy
sed -e '1i ## DO NOT EDIT\n## File managed by Ansible.\n\n' -i etc/tomcat/catalina.properties
sed -e '2i \\n\n<!-- ## DO NOT EDIT              -->\n<!-- ## File managed by Ansible. -->\n\n' -i etc/tomcat/context.xml
sed -e '2i \\n\n<!-- ## DO NOT EDIT              -->\n<!-- ## File managed by Ansible. -->\n\n' -i etc/tomcat/jaspic-providers.xml
sed -e '2i \\n\n<!-- ## DO NOT EDIT              -->\n<!-- ## File managed by Ansible. -->\n\n' -i etc/tomcat/jaspic-providers.xsd
sed -e '1i ## DO NOT EDIT\n## File managed by Ansible.\n\n' -i etc/tomcat/logging.properties
sed -e '2i \\n\n<!-- ## DO NOT EDIT              -->\n<!-- ## File managed by Ansible. -->\n\n' -i etc/tomcat/tomcat-users.xml
sed -e '2i \\n\n<!-- ## DO NOT EDIT              -->\n<!-- ## File managed by Ansible. -->\n\n' -i etc/tomcat/tomcat-users.xsd
sed -e '2i \\n\n<!-- ## DO NOT EDIT              -->\n<!-- ## File managed by Ansible. -->\n\n' -i etc/tomcat/web.xml
# NOTE: We use a template for server.xml, so delete for now
rm -v etc/tomcat/server.xml
# This file needs a newline at the end for linting rules
printf '\n' >> etc/tomcat/jaspic-providers.xsd

popd

################################################################

# https://src.fedoraproject.org/rpms/tomcat/tree/f34

pushd files/source/9

mkdir -pv opt/tomcat/libexec/
curl -o opt/tomcat/libexec/functions https://src.fedoraproject.org/rpms/tomcat/raw/f34/f/tomcat-functions
curl -o opt/tomcat/libexec/preamble https://src.fedoraproject.org/rpms/tomcat/raw/f34/f/tomcat-preamble
curl -o opt/tomcat/libexec/server https://src.fedoraproject.org/rpms/tomcat/raw/f34/f/tomcat-server
chmod -c 0644 opt/tomcat/libexec/functions
chmod -c 0755 opt/tomcat/libexec/{preamble,server}
sed -e 's,/usr/libexec/tomcat,/opt/tomcat/libexec,g' -e '3i ## DO NOT EDIT\n## File managed by Ansible.\n\n' -i opt/tomcat/libexec/functions
# Also remove reference to apache-commons-daemon jar
sed -e 's,/usr/libexec/tomcat,/opt/tomcat/libexec,g' -e '3i ## DO NOT EDIT\n## File managed by Ansible.\n\n' -e '/build-classpath commons-daemon/d' -i opt/tomcat/libexec/preamble
sed -e 's,/usr/libexec/tomcat,/opt/tomcat/libexec,g' -e '3i ## DO NOT EDIT\n## File managed by Ansible.\n\n' -i opt/tomcat/libexec/server

mkdir -pv etc/logrotate.d/
curl -o etc/logrotate.d/tomcat https://src.fedoraproject.org/rpms/tomcat/raw/f34/f/tomcat-9.0.logrotate
sed -e 's,@@@TCLOG@@@,/var/log/tomcat,g' -e '1i ## DO NOT EDIT\n## File managed by Ansible.\n\n' -i etc/logrotate.d/tomcat

mkdir -pv etc/systemd/system
curl -o etc/systemd/system/tomcat.service https://src.fedoraproject.org/rpms/tomcat/raw/f34/f/tomcat-9.0.service
curl -o etc/systemd/system/tomcat\@.service https://src.fedoraproject.org/rpms/tomcat/raw/f34/f/tomcat-named.service
sed -e 's,/usr/libexec/tomcat,/opt/tomcat/libexec,g' -e '1i ## DO NOT EDIT\n## File managed by Ansible.\n\n' -i etc/systemd/system/tomcat.service
sed -e 's,/usr/libexec/tomcat,/opt/tomcat/libexec,g' -e '1i ## DO NOT EDIT\n## File managed by Ansible.\n\n' -i etc/systemd/system/tomcat\@.service

# https://dl.fedoraproject.org/pub/fedora/linux/releases/34/Everything/x86_64/os/Packages/t/

mkdir -pv etc/sysconfig
rpm2cpio <( curl -fsSL "https://dl.fedoraproject.org/pub/fedora/linux/releases/34/Everything/x86_64/os/Packages/t/tomcat-9.0.44-1.fc34.noarch.rpm" ) | cpio --quiet --extract --to-stdout ./etc/sysconfig/tomcat > etc/sysconfig/tomcat
sed -e '1i ## DO NOT EDIT\n## File managed by Ansible.\n\n' -e '$ a \\n\n##\n## DO NOT EDIT\n##\n## Set variables in files in /etc/tomcat/conf.d/\n##' -i etc/sysconfig/tomcat

mkdir -pv etc/tomcat
rpm2cpio <( curl -fsSL "https://dl.fedoraproject.org/pub/fedora/linux/releases/34/Everything/x86_64/os/Packages/t/tomcat-9.0.44-1.fc34.noarch.rpm" ) | cpio --quiet --extract --to-stdout ./etc/tomcat/tomcat.conf > etc/tomcat/tomcat.conf
# Also remove option to use a library from the RPM that we don't have
sed \
  -e '1i ## DO NOT EDIT\n## File managed by Ansible.\n\n' \
  -e '$ a \\n\n##\n## DO NOT EDIT\n##\n## Set variables in files in /etc/tomcat/conf.d/\n##' \
  -e '/^$/{N;/^\n$/d;}' \
  -e '/javax.sql.DataSource/d' \
  -i etc/tomcat/tomcat.conf

# https://archive.apache.org/dist/tomcat/tomcat-9/?C=M;O=D

mkdir -pv etc/tomcat
curl https://archive.apache.org/dist/tomcat/tomcat-9/v9.0.86/bin/apache-tomcat-9.0.86.tar.gz | tar -f - -z -x -v -C etc/tomcat --strip 2 apache-tomcat-9.0.86/conf
sed -e '1i // DO NOT EDIT\n// File managed by Ansible.\n\n' -i etc/tomcat/catalina.policy
sed -e '1i ## DO NOT EDIT\n## File managed by Ansible.\n\n' -i etc/tomcat/catalina.properties
sed -e '2i \\n\n<!-- ## DO NOT EDIT              -->\n<!-- ## File managed by Ansible. -->\n\n' -i etc/tomcat/context.xml
sed -e '2i \\n\n<!-- ## DO NOT EDIT              -->\n<!-- ## File managed by Ansible. -->\n\n' -i etc/tomcat/jaspic-providers.xml
sed -e '2i \\n\n<!-- ## DO NOT EDIT              -->\n<!-- ## File managed by Ansible. -->\n\n' -i etc/tomcat/jaspic-providers.xsd
sed -e '1i ## DO NOT EDIT\n## File managed by Ansible.\n\n' -i etc/tomcat/logging.properties
sed -e '2i \\n\n<!-- ## DO NOT EDIT              -->\n<!-- ## File managed by Ansible. -->\n\n' -i etc/tomcat/tomcat-users.xml
sed -e '2i \\n\n<!-- ## DO NOT EDIT              -->\n<!-- ## File managed by Ansible. -->\n\n' -i etc/tomcat/tomcat-users.xsd
sed -e '2i \\n\n<!-- ## DO NOT EDIT              -->\n<!-- ## File managed by Ansible. -->\n\n' -i etc/tomcat/web.xml
# NOTE: We use a template for server.xml, so delete for now
rm -v etc/tomcat/server.xml
# These files need a newline at the end for linting rules
printf '\n' >> etc/tomcat/catalina.policy
printf '\n' >> etc/tomcat/jaspic-providers.xsd

popd


################################################################

pushd files/packages/7

# http://mirror.centos.org/centos/7/os/x86_64/Packages/

mkdir -pv etc/sysconfig
rpm2cpio <( curl -fsSL "http://mirror.centos.org/centos/7/os/x86_64/Packages/tomcat-7.0.76-15.el7.noarch.rpm" ) | cpio --quiet --extract --to-stdout ./etc/sysconfig/tomcat > etc/sysconfig/tomcat
sed -e '1i ## DO NOT EDIT\n## File managed by Ansible.\n\n' -e '$ a \\n\n##\n## DO NOT EDIT\n##\n## Set variables in files in /etc/tomcat/conf.d/\n##' -i etc/sysconfig/tomcat

mkdir -pv etc/tomcat
rpm2cpio <( curl -fsSL "http://mirror.centos.org/centos/7/os/x86_64/Packages/tomcat-7.0.76-15.el7.noarch.rpm" ) | cpio --quiet --extract --to-stdout ./etc/tomcat/tomcat.conf > etc/tomcat/tomcat.conf
sed -e '1i ## DO NOT EDIT\n## File managed by Ansible.\n\n' -e '$ a \\n\n##\n## DO NOT EDIT\n##\n## Set variables in files in /etc/tomcat/conf.d/\n##' -i etc/tomcat/tomcat.conf

popd


################################################################

find files/ templates/ -type f -exec chmod --changes u+rw,g+r,o+r '{}' \;
```
